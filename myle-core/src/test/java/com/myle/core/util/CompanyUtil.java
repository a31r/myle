package com.myle.core.util;

import com.myle.core.model.Company;
import com.myle.core.model.CompanyProperty;
import com.myle.core.model.enums.CompanyPropertyType;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Utility for manipulating with {@link Company} for tests
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class CompanyUtil extends BasicUtil {

    private final static String FIRST_COMPANY_NAME = "First Company";
    private final static String FIRST_COMPANY_ADDRESS = "First Company address";
    private final static String FIRST_COMPANY_PHONE = "First Company phone";
    private final static String FIRST_COMPANY_EMAIL = "First Company email";

    private final static String SECOND_COMPANY_NAME = "Second Company";
    private final static String SECOND_COMPANY_ADDRESS = "Second Company address";
    private final static String SECOND_COMPANY_PHONE = "Second Company phone";
    private final static String SECOND_COMPANY_EMAIL = "Second Company email";

    /**
     * Creates {@link Company}
     *
     * @return {@link Company}
     */
    public static Company createRandomCompany(EntityManager em) {
        Random random = new Random();
        int i = random.nextInt();
        String name = "Company "+ i + " name";
        String address = "Company "+ i + " address";
        String phone = "Company" + i + " phone";
        String email = "Company" + i + " email";
        return createCompany(em == null ? null : em, name, address, phone, email);
    }

    public static Company createRandomCompany() {
        return createRandomCompany(null);
    }

    /**
     * Creates {@link Company}
     *
     * @return {@link Company}
     */
    public static Company createFirstCompany(EntityManager em) {
        return createCompany(em == null ? null : em, FIRST_COMPANY_NAME, FIRST_COMPANY_ADDRESS,
                FIRST_COMPANY_PHONE, FIRST_COMPANY_EMAIL);
    }

    public static Company createFirstCompany() {
        return createFirstCompany(null);
    }

    /**
     * Creates {@link Company}
     *
     * @return {@link Company}
     */
    public static Company createSecondCompany(EntityManager em) {
        return createCompany(em == null ? null : em, SECOND_COMPANY_NAME, SECOND_COMPANY_ADDRESS,
                SECOND_COMPANY_PHONE, SECOND_COMPANY_EMAIL);
    }

    public static Company createSecondCompany() {
        return createSecondCompany(null);
    }

    /**
     * Creates {@link Company}
     *
     * @param name - name of {@link Company}
     * @param address - address of {@link Company}
     * @param phone - contact phone of {@link Company}
     * @param email - contact email of {@link Company}
     *
     * @return {@link Company}
     */
    private static Company createCompany(EntityManager em, String name, String address,
                                         String phone, String email) {
        Company company = new Company();
        company.setName(name);
        company.setConfirmed(true);

        List<CompanyProperty> properties = new ArrayList<>();
        properties.add(new CompanyProperty(CompanyPropertyType.ADDRESS, address));
        properties.add(new CompanyProperty(CompanyPropertyType.PHONE, phone));
        properties.add(new CompanyProperty(CompanyPropertyType.EMAIL, email));
        company.setProperties(properties);

        if (em != null)
            persist(em, company);

        return company;
    }

}
