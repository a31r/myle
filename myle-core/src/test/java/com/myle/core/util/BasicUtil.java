package com.myle.core.util;

import javax.persistence.EntityManager;

/**
 * Created by Админ on 18.10.2016.
 */
public class BasicUtil {

    protected static void persist(EntityManager em, Object o) {
        em.getTransaction().begin();
        em.persist(o);
        em.getTransaction().commit();
    }

}
