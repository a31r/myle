package com.myle.core.util;

import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.User;

import javax.persistence.EntityManager;
import java.util.Random;

/**
 * Utility for manipulating with {@link Customer} for tests
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class CustomerUtil extends BasicUtil {

    private static final String FIRST_CUSTOMER_NAME = "First customer name";
    private static final Company FIRST_CUSTOMER_COMPANY = CompanyUtil.createRandomCompany();
    private static final User FIRST_CUSTOMER_USER = UserUtil.createRandomUser();

    private static final String SECOND_CUSTOMER_NAME = "Second customer name";
    private static final Company SECOND_CUSTOMER_COMPANY = CompanyUtil.createRandomCompany();
    private static final User SECOND_CUSTOMER_USER = UserUtil.createRandomUser();

    /**
     * Create {@link Customer}
     * @return {@link Customer}
     */
    public static Customer createRandomCustomer(EntityManager em) {
        Random random = new Random();
        int i = random.nextInt();
        String name = "Customer " + i + " name";
        return create(em == null ? null : em, name, CompanyUtil.createRandomCompany(em), UserUtil.createRandomUser(em));
    }

    public static Customer createRandomCustomer() {
        return createRandomCustomer(null);
    }

    /**
     * Create {@link Customer}
     * @return {@link Customer}
     */
    public static Customer createFirstCustomer(EntityManager em) {
        return create(em == null ? null : em, FIRST_CUSTOMER_NAME, FIRST_CUSTOMER_COMPANY,
                FIRST_CUSTOMER_USER);
    }

    public static Customer createFirstCustomer() {
        return createFirstCustomer(null);
    }

    /**
     * Create {@link Customer}
     * @return {@link Customer}
     */
    public static Customer createSecondCustomer(EntityManager em) {
        return create(em == null ? null : em, SECOND_CUSTOMER_NAME, SECOND_CUSTOMER_COMPANY,
                SECOND_CUSTOMER_USER);
    }

    public static Customer createSecondCustomer() {
        return createSecondCustomer(null);
    }

    /**
     * Create {@link Customer}
     *
     * @param name - name of {@link Customer}
     * @param company - supplier {@link Company}
     * @return {@link Customer}
     */
    private static Customer create(EntityManager em, String name, Company company, User user) {

        Customer customer = new Customer(company, user);
        customer.setName(name);

        if (em != null)
           persist(em, customer);

        return customer;

    }
}
