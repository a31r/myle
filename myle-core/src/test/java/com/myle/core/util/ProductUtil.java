package com.myle.core.util;

import com.myle.core.model.Company;
import com.myle.core.model.Product;
import com.myle.core.model.ProductProperty;
import com.myle.core.model.enums.ProductPropertyType;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Utility for manipulating with {@link Product} for tests
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class ProductUtil extends BasicUtil {

    private static final String FIRST_PRODUCT_NAME = "First product name";
    private static final String FIRST_PRODUCT_PHONE = "First product phone";
    private static final Company FIRST_PRODUCT_COMPANY = CompanyUtil.createRandomCompany();

    private static final String SECOND_PRODUCT_NAME = "Second product name";
    private static final String SECOND_PRODUCT_PHONE = "Second product phone";
    private static final Company SECOND_PRODUCT_COMPANY = CompanyUtil.createRandomCompany();

    /**
     * Creates {@link Product}
     *
     * @return {@link Product}
     */
    public static Product createRandomProduct(EntityManager em) {
        Random random = new Random();
        int i = random.nextInt();
        String name = "Product " + i +" name ";
        String phone = "Product " + i +" phone ";
        return createProduct(em == null ? null : em, name, phone, CompanyUtil.createRandomCompany(em));
    }

    public static Product createRandomProduct() {
        return createRandomProduct(null);
    }

    /**
     * Creates {@link Product}
     *
     * @return {@link Product}
     */
    public static Product createFirstProduct(EntityManager em) {
        return createProduct(em == null ? null : em, FIRST_PRODUCT_NAME, FIRST_PRODUCT_PHONE, FIRST_PRODUCT_COMPANY);
    }

    public static Product createFirstProduct() {
        return createFirstProduct(null);
    }

    /**
     * Creates {@link Product}
     *
     * @return {@link Product}
     */
    public static Product createSecondProduct(EntityManager em) {
        return createProduct(em == null ? null : em, SECOND_PRODUCT_NAME, SECOND_PRODUCT_PHONE, SECOND_PRODUCT_COMPANY);
    }

    public static Product createSecondProduct() {
        return createSecondProduct(null);
    }

    /**
     * Creates {@link Product}
     *
     * @param name - name of {@link Product}
     * @param phone - contact phone of {@link Product}
     *
     * @return {@link Product}
     */
    private static Product createProduct(EntityManager em, String name, String phone, Company company) {

        Product product = new Product(company);

        List<ProductProperty> properties = new ArrayList<>();
        properties.add(new ProductProperty(ProductPropertyType.PHONE, phone));

        product.setName(name);
        product.setProperties(properties);

        if (em != null)
            persist(em, product);

        return product;

    }
}
