package com.myle.core.util;

import com.myle.core.model.Company;
import com.myle.core.model.Employee;
import com.myle.core.model.User;

import javax.persistence.EntityManager;
import java.util.Random;

/**
 * Utility for manipulating with {@link Employee} for tests
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class EmployeeUtil extends BasicUtil {

    private static final String FIRST_EMPLOYEE_NAME = "First employee name";
    private static final Company FIRST_EMPLOYEE_COMPANY = CompanyUtil.createRandomCompany();
    private static final User FIRST_EMPLOYEE_USER = UserUtil.createRandomUser();

    private static final String SECOND_EMPLOYEE_NAME = "Second employee name";
    private static final Company SECOND_EMPLOYEE_COMPANY = CompanyUtil.createRandomCompany();
    private static final User SECOND_EMPLOYEE_USER = UserUtil.createRandomUser();

    /**
     * Create {@link Employee}
     * @return {@link Employee}
     */
    public static Employee createFirstEmployee(EntityManager em) {
        return createEmployee(em == null ? null : em, FIRST_EMPLOYEE_NAME, FIRST_EMPLOYEE_COMPANY, FIRST_EMPLOYEE_USER);
    }

    public static Employee createFirstEmployee() {
        return createFirstEmployee(null);
    }

    /**
     * Create {@link Employee}
     * @return {@link Employee}
     */
    public static Employee createSecondEmployee(EntityManager em) {
        return createEmployee(em == null ? null : em, SECOND_EMPLOYEE_NAME, SECOND_EMPLOYEE_COMPANY, SECOND_EMPLOYEE_USER);
    }

    public static Employee createSecondEmployee() {
        return createSecondEmployee(null);
    }

    /**
     * Create {@link Employee} with random name and employer
     * @return {@link Employee}
     */
    public static Employee createRandomEmployee(EntityManager em) {

        Random random = new Random();
        int i = random.nextInt();
        String name = "Employee " + i + " name";

        return createEmployee(em == null ? null : em, name, CompanyUtil.createRandomCompany(em), UserUtil.createRandomUser(em));

    }

    public static Employee createRandomEmployee() {
        return createRandomEmployee(null);
    }

    /**
     * Create {@link Employee}
     *
     * @param name - name of {@link Employee}
     * @param company - employer [@link {@link Company}}
     * @return {@link Employee}
     */
    private static Employee createEmployee(EntityManager em, String name, Company company, User user) {

        Employee employee = new Employee(company, user);

        employee.setName(name);

        if (em != null)
            persist(em, employee);

        return employee;

    }

}
