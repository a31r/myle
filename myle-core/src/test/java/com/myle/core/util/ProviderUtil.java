package com.myle.core.util;

import com.myle.core.model.Product;
import com.myle.core.model.Provider;

import javax.persistence.EntityManager;
import java.util.Random;

/**
 * Utility for manipulating with {@link Provider} for tests
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class ProviderUtil extends BasicUtil {

    private static final String FIRST_PROVIDER_NAME = "First provider name";
    private static final Product FIRST_PROVIDER_PRODUCT = ProductUtil.createRandomProduct();

    private static final String SECOND_PROVIDER_NAME = "First provider name";
    private static final Product SECOND_PROVIDER_PRODUCT = ProductUtil.createRandomProduct();

    /**
     * Create {@link Provider}
     *
     * @return {@link Provider}
     */
    public static Provider createRandomProvider(EntityManager em) {
        Random random = new Random();
        int i = random.nextInt();
        String name = "Provider " + i + "name";
        return create(em == null ? null : em, name, ProductUtil.createRandomProduct(em));
    }

    public static Provider createRandomProvider() {
        return createRandomProvider(null);
    }

    /**
     * Create {@link Provider}
     *
     * @return {@link Provider}
     */
    public static Provider createFirstProvider(EntityManager em) {
        return create(em == null ? null : em, FIRST_PROVIDER_NAME, FIRST_PROVIDER_PRODUCT);
    }

    public static Provider createFirstProvider() {
        return createFirstProvider(null);
    }
    /**
     * Create {@link Provider}
     *
     * @return {@link Provider}
     */
    public static Provider createSecondProvider(EntityManager em) {
        return create(em == null ? null : em, SECOND_PROVIDER_NAME, SECOND_PROVIDER_PRODUCT);
    }

    public static Provider createSecondProvider() {
        return createSecondProvider(null);
    }
    /**
     * Create {@link Provider}
     *
     * @param name - name of provider
     *
     * @return {@link Provider}
     */
    private static Provider create(EntityManager em, String name, Product product) {

        Provider provider = new Provider(product);

        provider.setName(name);

        if (em != null)
            persist(em, provider);

        return provider;
    }

}
