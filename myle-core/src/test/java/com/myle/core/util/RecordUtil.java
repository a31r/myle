package com.myle.core.util;

import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.Provider;
import com.myle.core.model.Record;

import javax.persistence.EntityManager;
import java.util.Random;

/**
 * Utility for manipulating with {@link Record} for tests
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class RecordUtil extends BasicUtil {

    private static final String FIRST_RECORD_NAME = "First record name";
    private static final Provider FIRST_RECORD_PROVIDER = ProviderUtil.createRandomProvider();
    private static final Customer FIRST_RECORD_CUSTOMER = CustomerUtil.createRandomCustomer();

    private static final String SECOND_RECORD_NAME = "Second record name";
    private static final Provider SECOND_RECORD_PROVIDER = ProviderUtil.createRandomProvider();
    private static final Customer SECOND_RECORD_CUSTOMER = CustomerUtil.createRandomCustomer();

    /**
     * Create {@link Record}
     *
     * @return {@link Record}
     */
    public static Record createRandomRecord(EntityManager em) {

        Company company = CompanyUtil.createRandomCompany(em);
        Customer customer = CustomerUtil.createRandomCustomer(em);
        Provider provider = ProviderUtil.createRandomProvider(em);

        customer.setSupplier(company);
        provider.getProduct().setCompany(company);

        Random random = new Random();
        int i = random.nextInt();
        String name = "Record " + i + " name";
        return create(em == null ? null : em, name, provider, customer);
    }

    public static Record createRandomRecord() {
        return createRandomRecord(null);
    }

    /**
     * Create {@link Record}
     *
     * @return {@link Record}
     */
    public static Record createFirstRecord(EntityManager em) {
        return create(em == null ? null : em, FIRST_RECORD_NAME, FIRST_RECORD_PROVIDER, FIRST_RECORD_CUSTOMER);
    }

    public static Record createFirstRecord() {
        return createFirstRecord(null);
    }

    /**
     * Create {@link Record}
     *
     * @return {@link Record}
     */
    public static Record createSecondRecord(EntityManager em) {
        return create(em == null ? null : em, SECOND_RECORD_NAME, SECOND_RECORD_PROVIDER, SECOND_RECORD_CUSTOMER);
    }

    public static Record createSecondRecord() {
        return createSecondRecord(null);
    }

    /**
     * Create {@link Record}
     *
     * @param name - name of {@link Record}
     *
     * @return {@link Record}
     */
    private static Record create(EntityManager em, String name, Provider provider, Customer customer) {

        Record record = new Record(provider, customer);

        record.setName(name);

        if (em != null)
            persist(em, record);

        return record;

    }
}
