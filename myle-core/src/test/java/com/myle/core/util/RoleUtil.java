package com.myle.core.util;

import com.myle.core.model.Role;

/**
 * Utility for manipulating with {@link Role} for tests
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class RoleUtil {

    private final static String ADMIN_ROLE = "ROLE_ADMIN";
    private final static String USER_ROLE = "ROLE_USER";

    /**
     * Creates {@link Role}
     *
     * @param roleName - name of {@link Role}
     * @return {@link Role}
     */
    public static Role createRole(String roleName) {

        Role role = new Role();
        role.setName(roleName);
        return role;

    }

    /**
     * Creates {@link Role} for admin
     *
     * @return {@link Role}
     */
    public static Role createAdminRole() {

        Role role = new Role();
        role.setName(ADMIN_ROLE);
        return role;

    }

    /**
     * Creates {@link Role} for user
     *
     * @return {@link Role}
     */
    public static Role createUserRole() {

        Role role = new Role();
        role.setName(USER_ROLE);
        return role;

    }
}
