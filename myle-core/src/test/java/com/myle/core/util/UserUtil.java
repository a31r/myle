package com.myle.core.util;

import com.myle.core.model.Role;
import com.myle.core.model.User;
import com.myle.core.model.UserProperty;
import com.myle.core.model.enums.UserPropertyType;

import javax.persistence.EntityManager;
import java.util.*;

/**
 * Utility for manipulating with {@link User} for tests
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class UserUtil extends BasicUtil {

    private final static String FIRST_USER_USERNAME = "First user username";
    private final static String FIRST_USER_PASSWORD = "First user password";
    private final static String FIRST_USER_FIRST_NAME = "First user first name";
    private final static String FIRST_USER_LAST_NAME = "First user last name";
    private final static String FIRST_USER_EMAIL = "First user email";
    private final static String FIRST_USER_PHONE = "First user phone";
    private final static String FIRST_USER_ADDRESS = "First user address";

    private final static String SECOND_USER_USERNAME = "Second user username";
    private final static String SECOND_USER_PASSWORD = "Second user password";
    private final static String SECOND_USER_FIRST_NAME = "Second user first name";
    private final static String SECOND_USER_LAST_NAME = "Second user last name";
    private final static String SECOND_USER_EMAIL = "Second user email";
    private final static String SECOND_USER_PHONE = "Second user phone";
    private final static String SECOND_USER_ADDRESS = "Second user address";

    /**
     * Creates {@link User}
     *
     * @return {@link User}
     */
    public static User createRandomUser(EntityManager em) {

        Set<Role> roles = new HashSet<>();
        roles.add(RoleUtil.createUserRole());

        Random random = new Random();
        int i = random.nextInt();
        String firstName = "User " + i + " first name";
        String lastName = "User " + i + " last name";
        String username = "User " + i + " username";
        String password = "User " + i + " password";
        String address = "User " + i + " address";
        String phone = "User " + i + " phone";
        String email = "User " + i + " email";

        return createUser(em == null ? null : em, username, password, firstName,
                lastName, email, phone, address, roles);

    }

    public static User createRandomUser() {
        return createRandomUser(null);
    }

    /**
     * Creates {@link User}
     *
     * @return {@link User}
     */
    public static User createFirstUser(EntityManager em) {

        Set<Role> roles = new HashSet<>();
        roles.add(RoleUtil.createUserRole());

        return createUser(em == null ? null : em, FIRST_USER_USERNAME, FIRST_USER_PASSWORD, FIRST_USER_FIRST_NAME,
                FIRST_USER_LAST_NAME, FIRST_USER_EMAIL, FIRST_USER_PHONE, FIRST_USER_ADDRESS, roles);

    }

    public static User createFirstUser() {
        return createFirstUser(null);
    }

    /**
     * Creates {@link User}
     *
     * @return {@link User}
     */
    public static User createSecondUser(EntityManager em) {

        Set<Role> roles = new HashSet<>();
        roles.add(RoleUtil.createUserRole());
        roles.add(RoleUtil.createAdminRole());

        return createUser(em == null ? null : em, SECOND_USER_USERNAME, SECOND_USER_PASSWORD, SECOND_USER_FIRST_NAME,
                SECOND_USER_LAST_NAME, SECOND_USER_EMAIL, SECOND_USER_PHONE, SECOND_USER_ADDRESS, roles);

    }

    public static User createSecondUser() {
        return createSecondUser(null);
    }

    /**
     * Creates {@link User}
     *
     * @param username - {@link User}s username
     * @param password - {@link User}s password
     * @param firstName - {@link User}s first name
     * @param lastName - {@link User}s last name
     * @param email - {@link User}s email
     * @param phone - {@link User}s phone
     * @param roles - {@link User}s roles
     * @return  - {@link User}
     */
    private static User createUser(EntityManager em, String username, String password, String firstName,
                    String lastName, String email, String phone, String address, Set<Role> roles) {
        User user = new User();

        List<UserProperty> props = new ArrayList<>();
        props.add(new UserProperty(UserPropertyType.ADDRESS, address));

        user.setUsername(username);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPhone(phone);
        user.setRoles(roles);
        user.setProperties(props);

        if (em != null)
            persist(em, user);

        return user;
    }
}
