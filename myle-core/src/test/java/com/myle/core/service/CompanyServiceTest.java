package com.myle.core.service;

import com.myle.core.config.TestDataBaseConfig;
import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.Employee;
import com.myle.core.util.CompanyUtil;
import com.myle.core.util.CustomerUtil;
import com.myle.core.util.EmployeeUtil;
import com.myle.core.util.UserUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Simple test class for {@link CompanyService} implementation
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@Transactional
public class CompanyServiceTest {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private CompanyService companyService;
    private Company company;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
        company = CompanyUtil.createRandomCompany(em);
    }

    @Test
    public void testAddCompany() throws Exception {
        Company company = CompanyUtil.createRandomCompany(em);
        Assert.assertNotNull(company);
    }

    @Test
    public void testEditCompany() throws Exception {
        boolean confirmed = company.getConfirmed();
        company.setConfirmed(!confirmed);
        company = companyService.edit(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(!confirmed == company.getConfirmed());
    }

    @Test
    public void testDeleteCompanyById() throws Exception {
        Long id = company.getId();
        if (id != null)
            companyService.delete(id);
    }

    @Test
    public void testDeleteCompany() throws Exception {
        companyService.delete(company);
    }

    @Test
    public void testGetCompanyById() throws Exception {
        Assert.assertNotNull(companyService.getById(company.getId()));
    }

    @Test
    public void testGetSupplierCompaniesEmpty() throws Exception {
        Assert.assertTrue(companyService.getSupplierCompanies(UserUtil.createRandomUser(em)).isEmpty());
    }

    @Test
    public void testGetSupplierCompaniesNotEmpty() throws Exception {
        Customer customer = CustomerUtil.createRandomCustomer(em);
        Assert.assertFalse(companyService.getSupplierCompanies(customer.getUser()).isEmpty());
    }

    @Test
    public void testGetEmployerCompaniesEmpty() throws Exception {
        Assert.assertTrue(companyService.getEmployerCompanies(UserUtil.createRandomUser(em)).isEmpty());
    }

    @Test
    public void testGetEmployerCompaniesNotEmpty() throws Exception {
        Employee employee = EmployeeUtil.createRandomEmployee(em);
        Assert.assertFalse(companyService.getEmployerCompanies(employee.getUser()).isEmpty());
    }

    @Test
    public void testDeleteUsersCompanyUserIsNotAdmin() throws Exception {
        companyService.deleteUsersCompany(CompanyUtil.createRandomCompany(em),UserUtil.createRandomUser(em));
    }

    @Test
    public void testDeleteUsersCompanyUserIsAdmin() throws Exception {
        Employee employee = EmployeeUtil.createRandomEmployee(em);
        companyService.deleteUsersCompany(employee.getEmployer(), employee.getUser());
    }

    @Test
    public void testGetUsersCompaniesEmpty() throws Exception {
        Assert.assertTrue(companyService.getEmployerCompanies(UserUtil.createRandomUser(em)).isEmpty());
    }

    @Test
    public void testGetUsersCompaniesNotEmpty() throws Exception {
        Employee employee = EmployeeUtil.createRandomEmployee(em);
        Assert.assertFalse(companyService.getEmployerCompanies(employee.getUser()).isEmpty());
    }

}
