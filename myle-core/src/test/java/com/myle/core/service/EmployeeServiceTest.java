package com.myle.core.service;

import com.myle.core.config.TestDataBaseConfig;
import com.myle.core.model.Company;
import com.myle.core.model.Employee;
import com.myle.core.model.User;
import com.myle.core.util.CompanyUtil;
import com.myle.core.util.EmployeeUtil;
import com.myle.core.util.UserUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Simple test class for {@link EmployeeService} implementation
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@Transactional
public class EmployeeServiceTest {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private EmployeeService employeeService;
    private Employee employee;
    private Company company;
    private User user;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
        employee = EmployeeUtil.createRandomEmployee(em);
        company = employee.getEmployer();
        user = employee.getUser();
    }

    @Test
    public void testAddEmployee() throws Exception {
        Company company = CompanyUtil.createRandomCompany(em);
        User user = UserUtil.createRandomUser(em);
        employeeService.add(new Employee(company, user));
    }

    @Test
    public void testEditEmployee() throws Exception {
        employee.setName("New name");
        employeeService.edit(employee);
    }

    @Test
    public void testDeleteEmployeeById() throws Exception {
        Long id = employee.getId();
        if (id != null)
            employeeService.delete(id);
    }

    @Test
    public void testDeleteEmployee() throws Exception {
        if (employee.getEmployer().getId() != null)
            employeeService.delete(employee);
    }

    @Test
    public void testGetEmployeeById() throws Exception {
        employeeService.getById(1l);
    }

    @Test
    public void testIsCustomerOfCompanyFalse() throws Exception{
        Assert.assertFalse(employeeService.isEmployeeOfCompany(CompanyUtil.createRandomCompany(em), UserUtil.createRandomUser(em)));
    }

    @Test
    public void testIsCustomerOfCompanyTrue() throws Exception{
        Assert.assertTrue(employeeService.isEmployeeOfCompany(company, user));
    }

    @Test
    public void testGetEmployeeOfCompanyUserIsNotEmployee() throws Exception {
        Assert.assertNull(employeeService.getEmployeeOfCompany(CompanyUtil.createRandomCompany(em), UserUtil.createRandomUser(em)));
    }

    @Test
    public void testGetEmployeeOfCompanyUserIsEmployee() throws Exception {
        Assert.assertNotNull(employeeService.getEmployeeOfCompany(company, user));
    }

    @Test
    public void testAddEmployeeToCompany() throws Exception {
        Company company = CompanyUtil.createRandomCompany(em);
        User user = UserUtil.createRandomUser(em);
        employeeService.addEmployeeToCompany(company, user);
    }

    @Test
    public void testDeleteEmployeeOfCompanyUserIsNotEmployee() throws Exception {
        employeeService.deleteEmployeeOfCompany(CompanyUtil.createRandomCompany(em), UserUtil.createRandomUser(em));
    }

    @Test
    public void testDeleteEmployeeOfCompanyUserIsEmployee() throws Exception {
        employeeService.deleteEmployeeOfCompany(company, user);
    }

    @Test
    public void testGetEmployeesOfCompanyEmpty() throws Exception {
        Assert.assertTrue(employeeService.getEmployeesOfCompany(CompanyUtil.createRandomCompany(em)).isEmpty());
    }

    @Test
    public void testGetEmployeesOfCompanyNotEmpty() throws Exception {
        Assert.assertFalse(employeeService.getEmployeesOfCompany(company).isEmpty());
    }

    @Test
    public void testGetEmployeeProfilesOfUserEmpty() throws Exception {
        Assert.assertTrue(employeeService.getEmployeeProfilesOfUser(UserUtil.createRandomUser(em)).isEmpty());
    }

    @Test
    public void testGetEmployeeProfilesOfUserNotEmpty() throws Exception {
        Assert.assertFalse(employeeService.getEmployeeProfilesOfUser(user).isEmpty());
    }
}
