package com.myle.core.service;

import com.myle.core.config.TestDataBaseConfig;
import com.myle.core.model.*;
import com.myle.core.util.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Simple test class for {@link RecordService} implementation
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@Transactional
public class RecordServiceTest {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private RecordService recordService;
    private Record record;
    private Provider provider;
    private Customer customer;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
        record = RecordUtil.createRandomRecord(em);
        provider = record.getProvider();
        customer = record.getCustomer();
    }

    @Test
    public void testAddRecord() throws Exception {
        final Provider provider = ProviderUtil.createRandomProvider(em);
        final Customer customer = CustomerUtil.createRandomCustomer(em);
        Assert.assertNotNull(recordService.add(new Record(provider, customer)));
    }

    @Test
    public void testEditRecord() throws Exception {
        String newName = "New name";
        record.setName(newName);
        record = recordService.edit(record);
        Assert.assertTrue(newName.equals(record.getName()));
    }

    @Test
    public void testDeleteRecordById() throws Exception {
        Long id = record.getId();
        if (id != null)
            recordService.delete(id);
    }

    @Test
    public void testDeleteRecord() throws Exception {
            recordService.delete(record);
    }

    @Test
    public void testGetRecordById() throws Exception {
        Assert.assertNotNull(recordService.getById(record.getId()));
    }

    @Test
    public void testDeleteRecordOfProvider() throws Exception {
        recordService.deleteRecordOfProvider(provider, record);
    }

    @Test
    public void testGetRecordsOfUserInCompanyEmpty() throws Exception {
        Assert.assertTrue(recordService.getRecordsOfUserInCompany(CompanyUtil.createRandomCompany(em), UserUtil.createRandomUser(em)).isEmpty());
    }

    @Test
    public void testGetRecordsOfUserInCompanyNotEmpty() throws Exception {
        final Company company = provider.getProduct().getCompany();
        final User user = customer.getUser();
        Assert.assertFalse(recordService.getRecordsOfUserInCompany(company, user).isEmpty());
    }

    @Test
    public void testGetRecordsOfUserInProductEmpty() throws Exception {
        Assert.assertTrue(recordService.getRecordsOfUserInProduct(ProductUtil.createRandomProduct(em), UserUtil.createRandomUser(em)).isEmpty());
    }

    @Test
    public void testGetRecordsOfUserInProductNotEmpty() throws Exception {
        final Product product = provider.getProduct();
        final User user = customer.getUser();
        Assert.assertFalse(recordService.getRecordsOfUserInProduct(product, user).isEmpty());
    }

    @Test
    public void testGetRecordsOfUserInProviderEmpty() throws Exception {
        Assert.assertTrue(recordService.getRecordsOfUserInProvider(ProviderUtil.createRandomProvider(em), UserUtil.createRandomUser(em)).isEmpty());
    }

    @Test
    public void testGetRecordsOfUserInProviderNotEmpty() throws Exception {
        final User user = customer.getUser();
        Assert.assertFalse(recordService.getRecordsOfUserInProvider(provider, user).isEmpty());
    }

    @Test
    public void testGetRecordsOfCompanyEmpty() throws Exception {
        Assert.assertTrue(recordService.getRecordsOfCompany(CompanyUtil.createRandomCompany(em)).isEmpty());
    }

    @Test
    public void testGetRecordsOfCompanyNotEmpty() throws Exception {
        final Company company = provider.getProduct().getCompany();
        Assert.assertFalse(recordService.getRecordsOfCompany(company).isEmpty());
    }

    @Test
    public void testGetRecordsOfProductEmpty() throws Exception {
        Assert.assertTrue(recordService.getRecordsOfProduct(ProductUtil.createRandomProduct(em)).isEmpty());
    }

    @Test
    public void testGetRecordsOfProductNotEmpty() throws Exception {
        final Product product = provider.getProduct();
        Assert.assertFalse(recordService.getRecordsOfProduct(product).isEmpty());
    }

    @Test
    public void testGetRecordsOfProviderEmpty() throws Exception {
        Assert.assertTrue(recordService.getRecordsOfProvider(ProviderUtil.createRandomProvider(em)).isEmpty());
    }

    @Test
    public void testGetRecordsOfProviderNotEmpty() throws Exception {
        Assert.assertFalse(recordService.getRecordsOfProvider(provider).isEmpty());
    }
}
