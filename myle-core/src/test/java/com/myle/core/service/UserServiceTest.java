package com.myle.core.service;

import com.myle.core.config.TestDataBaseConfig;
import com.myle.core.model.Company;
import com.myle.core.model.User;
import com.myle.core.util.CompanyUtil;
import com.myle.core.util.CustomerUtil;
import com.myle.core.util.UserUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Simple test class for {@link UserService} implementation
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@WebAppConfiguration
public class UserServiceTest {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private UserService userService;
    private User user;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
        user = UserUtil.createRandomUser(em);
    }

    @Test
    public void testAddUser() throws Exception {
        Assert.assertNotNull(userService.add(UserUtil.createRandomUser()));
    }

    @Test
    public void testEditUser() throws Exception {
        final String newUserName = "New username";
        user.setUsername(newUserName);
        user = userService.edit(user);
        Assert.assertNotNull(user);
        Assert.assertTrue(newUserName.equals(user.getUsername()));
    }

    @Test
    public void testDeleteUser() throws Exception {
        userService.delete(user);
    }

    @Test
    public void testDeleteUserById() throws Exception {
        userService.delete(user.getId());
    }

    @Test
    public void testGetUserByUsername() throws Exception {
        Assert.assertNotNull(userService.getUserByUsername(user.getUsername()));
    }

    @Test
    public void testGetUserByPhone() throws Exception {
        Assert.assertNotNull(userService.getUserByPhone(user.getPhone()));
    }

    @Test
    public void testGetUserById() throws Exception {
        Assert.assertNotNull(userService.getById(user.getId()));
    }

    @Test
    public void testGetAuthenticatedUser() throws Exception {
        try {
            userService.getAuthenticatedUser();
        } catch (SecurityException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
