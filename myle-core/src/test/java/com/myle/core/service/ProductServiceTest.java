package com.myle.core.service;

import com.myle.core.config.TestDataBaseConfig;
import com.myle.core.model.Company;
import com.myle.core.model.Product;
import com.myle.core.util.CompanyUtil;
import com.myle.core.util.ProductUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Simple test class for {@link ProductService} implementation
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@Transactional
public class ProductServiceTest {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private ProductService productService;
    private Product product;
    private Company company;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
        product = ProductUtil.createRandomProduct(em);
        company = product.getCompany();
    }

    @Test
    public void testAddProduct() throws Exception {
        Company company = CompanyUtil.createRandomCompany(em);
        Product product = new Product(company);
        product = productService.add(product);
        Assert.assertNotNull(product);
    }

    @Test
    public void testEditProduct() throws Exception {
        String newName = "New name";
        product.setName(newName);
        product = productService.edit(product);
        Assert.assertNotNull(product);
        Assert.assertTrue(newName.equals(product.getName()));
    }

    @Test
    public void testDeleteProductById() throws Exception {
        Long id = product.getId();
        if (id != null)
            productService.delete(id);
    }

    @Test
    public void testDeleteProduct() throws Exception {
        productService.delete(product);
    }

    @Test
    public void testGetProductById() throws Exception {
        Assert.assertNotNull(productService.getById(1l));
    }

    @Test
    public void testDeleteProductOfCompany() throws Exception {
        productService.deleteProductOfCompany(CompanyUtil.createRandomCompany(), ProductUtil.createRandomProduct());
    }

    @Test
    public void testGetProductsOfCompanyEmpty() throws Exception {
        Assert.assertTrue(productService.getProductsOfCompany(CompanyUtil.createRandomCompany(em)).isEmpty());
    }

    @Test
    public void testGetProductsOfCompanyNotEmpty() throws Exception {
        Assert.assertFalse(productService.getProductsOfCompany(company).isEmpty());
    }
}
