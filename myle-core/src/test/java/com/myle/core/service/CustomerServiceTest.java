package com.myle.core.service;

import com.myle.core.config.TestDataBaseConfig;
import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.User;
import com.myle.core.util.CompanyUtil;
import com.myle.core.util.CustomerUtil;
import com.myle.core.util.UserUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Simple test class for {@link CustomerService} implementation
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@Transactional
public class CustomerServiceTest {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private CustomerService customerService;
    private Customer customer;
    private Company company;
    private User user;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
        customer = CustomerUtil.createRandomCustomer(em);
        company = customer.getSupplier();
        user = customer.getUser();
    }

    @Test
    public void testAddCustomer() throws Exception {
        Company company = CompanyUtil.createRandomCompany(em);
        User user = UserUtil.createRandomUser(em);
        Assert.assertNotNull(customerService
                .add(new Customer(company, user)));
    }

    @Test
    public void testEditCustomer() throws Exception {
        customer.setName("New name");
        Assert.assertNotNull(customerService.edit(customer));
    }

    @Test
    public void testDeleteCustomerById() throws Exception {
        Long id = customer.getId();
        if (id != null)
            customerService.delete(id);
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        customerService.delete(customer);
    }

    @Test
    public void testGetCustomerById() throws Exception {
        customerService.getById(customer.getId());
    }

    @Test
    public void testIsCustomerOfCompanyFalse() throws Exception{
        Assert.assertFalse(customerService
                .isCustomerOfCompany(CompanyUtil.createFirstCompany(em), UserUtil.createFirstUser(em)));
    }

    @Test
    public void testIsCustomerOfCompanyTrue() throws Exception{
        Assert.assertTrue(customerService
                .isCustomerOfCompany(company, user));
    }

    @Test
    public void testGetCustomerOfCompanyNull() throws Exception {
        Assert.assertNull(customerService
                .getCustomerOfCompany(CompanyUtil.createRandomCompany(em), UserUtil.createRandomUser(em)));
    }

    @Test
    public void testGetCustomerOfCompanyNotNull() throws Exception {
        Assert.assertNotNull(customerService
                .getCustomerOfCompany(company, user));
    }

    @Test
    public void testAddCustomerToCompany() throws Exception {
        Company company = CompanyUtil.createRandomCompany(em);
        User user = UserUtil.createRandomUser(em);
        Assert.assertNotNull(customerService
                .addCustomerToCompany(company, user));
    }

    @Test
    public void testDeleteCustomerOfCompanyUserIsNotCustomer() throws Exception {
        customerService
                .deleteCustomerOfCompany(CompanyUtil.createRandomCompany(em), UserUtil.createRandomUser(em));
    }

    @Test
    public void testDeleteCustomerOfCompanyUserIsCustomer() throws Exception {
        customerService
                .deleteCustomerOfCompany(company, user);
    }

    @Test
    public void testGetCustomersOfCompanyEmpty() throws Exception {
        Assert.assertTrue(customerService
                .getCustomersOfCompany(CompanyUtil.createRandomCompany(em)).isEmpty());
    }

    @Test
    public void testGetCustomersOfCompanyNotEmpty() throws Exception {
        Assert.assertFalse(customerService.getCustomersOfCompany(company).isEmpty());
    }

    @Test
    public void testGetCustomersOfUserEmpty() throws Exception {
        Assert.assertTrue(customerService.getCustomersOfUser(UserUtil.createRandomUser(em)).isEmpty());
    }

    @Test
    public void testGetCustomersOfUserNotEmpty() throws Exception {
        Assert.assertFalse(customerService.getCustomersOfUser(user).isEmpty());
    }

}
