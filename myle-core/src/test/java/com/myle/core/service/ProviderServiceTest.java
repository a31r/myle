package com.myle.core.service;

import com.myle.core.config.TestDataBaseConfig;
import com.myle.core.model.Product;
import com.myle.core.model.Provider;
import com.myle.core.util.ProductUtil;
import com.myle.core.util.ProviderUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Simple test class for {@link ProviderService} implementation
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@Transactional
public class ProviderServiceTest {

    @Resource
    private EntityManagerFactory emf;
    private EntityManager em;

    @Resource
    private ProviderService providerService;
    private Provider provider;
    private Product product;

    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
        provider = ProviderUtil.createRandomProvider(em);
        product = provider.getProduct();
    }

    @Test
    public void testAddProvider() throws Exception {
        Product product = ProductUtil.createRandomProduct(em);
        Provider provider = new Provider(product);
        Assert.assertNotNull(providerService.add(provider));
    }

    @Test
    public void testEditProvider() throws Exception {
        String newName = "New name";
        provider.setName(newName);
        provider = providerService.edit(provider);
        Assert.assertTrue(newName.equals(provider.getName()));
    }

    @Test
    public void testDeleteProviderById() throws Exception {
        Long id = provider.getId();
        if (id != null)
            providerService.delete(id);
    }

    @Test
    public void testDeleteProvider() throws Exception {
        providerService.delete(provider);
    }

    @Test
    public void testGetProviderById() throws Exception {
        Assert.assertNotNull(providerService.getById(provider.getId()));
    }

    @Test
    public void testDeleteProviderOfProduct() throws Exception {
        providerService.deleteProviderOfProduct(product, provider);
    }

    @Test
    public void testGetProvidersOfProductEmpty() throws Exception {
        Assert.assertTrue(providerService.getProvidersOfProduct(ProductUtil.createRandomProduct(em)).isEmpty());
    }

    @Test
    public void testGetProvidersOfProductNotEmpty() throws Exception {
        Assert.assertFalse(providerService.getProvidersOfProduct(product).isEmpty());
    }

}
