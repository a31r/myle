package com.myle.core.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity of a {@link Customer}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "customers")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id", nullable = false)
    private Company supplier;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /**
     * Default constructor
     */
    public Customer() {
    }

    /**
     * Constructor with supplier
     *
     * @param supplier - {@link Company}
     * @param user - {@link User}
     */
    public Customer(Company supplier, User user) {
        this.supplier = supplier;
        this.user = user;
    }

    /**
     * Gets id of {@link Customer}
     *
     * @return id of {@link Customer}
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id of {@link Customer}
     *
     * @param id - {@link Customer}s id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name of {@link Customer}
     *
     * @return name of {@link Customer}
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of {@link Customer}
     *
     * @param name - {@link Customer}s name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets supplier of {@link Customer}
     * @see Company
     *
     * @return supplier of {@link Customer}
     */
    public Company getSupplier() {
        return supplier;
    }

    /**
     * Sets supplier of {@link Customer}
     * @see Company
     *
     * @param supplier - {@link Customer}s supplier
     */
    public void setSupplier(Company supplier) {
        this.supplier = supplier;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", supplier=" + supplier +
                ", user=" + user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (id != null ? !id.equals(customer.id) : customer.id != null) return false;
        if (supplier != null ? !supplier.equals(customer.supplier) : customer.supplier != null) return false;
        return user != null ? user.equals(customer.user) : customer.user == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (supplier != null ? supplier.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }
}
