package com.myle.core.model.validator;

import com.myle.core.model.Company;
import com.myle.core.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Simple validator for {@link Company}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class CompanyValidator implements Validator {

    @Autowired
    private CompanyService companyService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Company.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {

        Company company = (Company) o;
        String name = company.getName();

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "registrationForm.required");

    }
}
