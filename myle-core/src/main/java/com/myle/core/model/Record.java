package com.myle.core.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity of a {@link Record}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "records")
public class Record implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id", nullable = false)
    private Provider provider;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    /**
     * Default constructor
     */
    public Record() {
    }

    /**
     * Constructor with params
     *
     * @param provider - {@link Provider}
     */
    public Record(Provider provider, Customer customer) {
        this.provider = provider;
        this.customer = customer;
    }

    /**
     * Gets id of {@link Record}
     *
     * @return id of {@link Record}
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id of {@link Record}
     *
     * @param id - {@link Record}s id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name of {@link Record}
     *
     * @return name of {@link Record}
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of {@link Record}
     *
     * @param name - {@link Record}s name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets {@link Provider} of record
     * @see Provider
     *
     * @return - a {@link Provider}
     */
    public Provider getProvider() {
        return provider;
    }

    /**
     * Gets {@link Provider} of record
     * @see Provider
     *
     * @param provider - {@link Provider}
     */
    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    /**
     * Gets {@link Customer} of record
     * @see Customer
     *
     * @return - {@link Customer}
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets {@link Customer} of record
     * @see Customer
     *
     * @param customer - {@link Customer}
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", provider=" + provider +
                ", customer=" + customer +
                ", employee=" + employee +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (id != null ? !id.equals(record.id) : record.id != null) return false;
        if (name != null ? !name.equals(record.name) : record.name != null) return false;
        if (provider != null ? !provider.equals(record.provider) : record.provider != null) return false;
        if (customer != null ? !customer.equals(record.customer) : record.customer != null) return false;
        return employee != null ? employee.equals(record.employee) : record.employee == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (provider != null ? provider.hashCode() : 0);
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (employee != null ? employee.hashCode() : 0);
        return result;
    }
}
