package com.myle.core.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Simple entity that represents an {@link Employee} of a {@link Company}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "employees")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id", nullable = false)
    private Company employer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /**
     * Default constructor
     */
    public Employee() {
    }

    /**
     * Constructor with employer
     *
     * @param employer - {@link Company}
     * @param user - {@link User}
     */
    public Employee(Company employer, User user) {
        this.employer = employer;
        this.user = user;
    }

    /**
     * Gets id of {@link Employee}
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id of {@link Employee}
     *
     * @param id - id of {@link Employee}
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name of {@link Employee}
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of {@link Employee}
     *
     * @param name - name of {@link Employee}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets employer of {@link Employee}
     * @see Company
     *
     * @return employer {@link Company}
     */
    public Company getEmployer() {
        return employer;
    }

    /**
     * Sets employer of {@link Employee}
     * @see Company
     *
     * @param employer - employer {@link Company}
     */
    public void setEmployer(Company employer) {
        this.employer = employer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", employer=" + employer +
                ", user=" + user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (id != null ? !id.equals(employee.id) : employee.id != null) return false;
        if (employer != null ? !employer.equals(employee.employer) : employee.employer != null) return false;
        return user != null ? user.equals(employee.user) : employee.user == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (employer != null ? employer.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }
}
