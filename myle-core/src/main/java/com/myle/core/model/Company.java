package com.myle.core.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity of a {@link Company}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "companies")
public class Company implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column
    private boolean confirmed;


    @OneToMany(targetEntity = CompanyProperty.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "company_id")
    private List<CompanyProperty> properties = new ArrayList<>();

    /**
     * Gets id of {@link Company}
     *
     * @return id of {@link Company}
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id of {@link Company}
     *
     * @param id - {@link Company}s id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name of {@link Company}
     *
     * @return name of {@link Company}
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of {@link Company}
     *
     * @param name - {@link Company}s name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets flag for confirmed {@link Company}
     *
     * @return flag for confirmed {@link Company}
     */
    public boolean getConfirmed() {
        return confirmed;
    }

    /**
     * Sets flag for confirmed {@link Company}
     *
     * @param confirmed - flag for confirmed {@link Company}
     */
    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     * Gets properties of {@link Company}
     * @see CompanyProperty
     *
     * @return list of {@link CompanyProperty}
     */
    public List<CompanyProperty> getProperties() {
        return properties;
    }

    /**
     * Sets properties of {@link Company}
     * @see CompanyProperty
     *
     * @param properties - list of {@link CompanyProperty}
     */
    public void setProperties(List<CompanyProperty> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", confirmed=" + confirmed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Company company = (Company) o;

        if (confirmed != company.confirmed) return false;
        if (id != null ? !id.equals(company.id) : company.id != null) return false;
        return name != null ? name.equals(company.name) : company.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (confirmed ? 1 : 0);
        return result;
    }
}
