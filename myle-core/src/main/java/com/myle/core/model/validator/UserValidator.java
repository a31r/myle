package com.myle.core.model.validator;

import com.myle.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.myle.core.model.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple validator for {@link User}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {

        User user = (User) o;
        String username = user.getUsername();
        String phone = user.getPhone();
        String password = user.getPassword();

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "registrationForm.required");

        if (userService.getUserByUsername(username) != null)
            errors.rejectValue("username", "registrationForm.username.non-unique");
        if (username.length() < 4 || username.length() > 31)
            errors.rejectValue("username", "registrationForm.username.size");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "registrationForm.required");

        if (userService.getUserByPhone(phone) != null || invalidPhoneNumber(phone))
            errors.rejectValue("phone", "registrationForm.phone.invalid");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "registrationForm.required");

        if (password.length() < 6 || password.length() > 31)
            errors.rejectValue("password", "registrationForm.password.size");

    }

    boolean invalidPhoneNumber(String phone) {
        Pattern pattern = Pattern.compile("^(8|\\+7)?\\d{10}$");
        Matcher matcher = pattern.matcher(phone);
        return !matcher.matches();
    }
}
