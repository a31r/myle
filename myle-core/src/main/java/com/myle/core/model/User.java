package com.myle.core.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Entity of a {@link User}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Transient
    private String confirmPassword;


    @Column(name = "first_name")
    private String firstName = "First name";

    @Column(name = "last_name")
    private String lastName = "Last name";

    @Transient
    private String fullName = this.firstName + " " + this.lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone", nullable = false, unique = true)
    private String phone;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();


    @OneToMany(targetEntity = UserProperty.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id")
    private List<UserProperty> properties = new ArrayList<>();

    /**
     * Get id of {@link User}
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set id to {@link User}
     *
     * @param id - id of {@link User}
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get username of {@link User}
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set username to {@link User}
     *
     * @param username - username of {@link User}
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get password of {@link User}
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set password to {@link User}
     *
     * @param password - password of {@link User}
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get confirm password of {@link User}
     *
     * @return confirm password
     */
    public String getConfirmPassword() {
        return this.confirmPassword;
    }

    /**
     * Set password to {@link User}
     *
     * @param confirmPassword - confirm password of {@link User}
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Get first name of {@link User}
     *
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set first name to {@link User}
     *
     * @param firstName - first name of {@link User}
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get last name of {@link User}
     *
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set last name to {@link User}
     *
     * @param lastName - last name of {@link User}
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets full name
     *
     * @return - full name
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets full name
     *
     * @param fullName - full name
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * Get email of {@link User}
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set email to {@link User}
     *
     * @param email - email of {@link User}
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get phone of {@link User}
     *
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set phone to {@link User}
     *
     * @param phone - phone of {@link User}
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Get roles of {@link User}
     *
     * @see Role
     * @return set of {@link Role}s
     */
    public Set<Role> getRoles() {
        return roles;
    }

    /**
     * Set roles for {@link User}
     *
     * @see Role
     * @param roles - set of {@link Role}
     */
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * Gets properties of {@link User}
     * @see UserProperty
     *
     * @return list of {@link UserProperty}
     */
    public List<UserProperty> getProperties() {
        return properties;
    }

    /**
     * Sets properties of {@link User}
     * @see UserProperty
     *
     * @param properties - list of {@link UserProperty}
     */
    public void setProperties(List<UserProperty> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (username != null ? !username.equals(user.username) : user.username != null) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        return phone != null ? phone.equals(user.phone) : user.phone == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }
}
