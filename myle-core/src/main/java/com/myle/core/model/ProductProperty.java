package com.myle.core.model;

import com.myle.core.model.enums.ProductPropertyType;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Implementation of {@link BasicProperty} interface for {@link Product}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "properties_of_products")
public class ProductProperty extends BasicProperty<ProductPropertyType> {

    /**
     * Default constructor
     */
    public ProductProperty() {
    }

    /**
     * Constructor with type and value
     *
     * @param type - type of property of {@link Product}
     *             @see ProductPropertyType
     * @param value - value of property of {@link Product}
     */
    public ProductProperty(ProductPropertyType type, String value) {
        this.setType(type);
        this.setValue(value);
    }
}
