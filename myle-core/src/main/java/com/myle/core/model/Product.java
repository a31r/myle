package com.myle.core.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity of a {@link Product}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "products")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @OneToMany(targetEntity = ProductProperty.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "product_id")
    private List<ProductProperty> properties = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    /**
     * Default constructor
     */
    public Product() {
    }

    /**
     * Constructor with params
     *
     * @param company - producer {@link Company}
     */
    public Product(Company company) {
        this.company = company;
    }

    /** Get id of {@link Product}
     *
     * @return id of {@link Product}
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id of {@link Product}
     * @param id - id of {@link Product}
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name of {@link Product}
     *
     * @return name of {@link Product}
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of {@link Product}
     *
     * @param name - name of {@link Product}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets properties of {@link Product}
     * @see ProductProperty
     *
     * @return list of {@link ProductProperty}
     */
    public List<ProductProperty> getProperties() {
        return properties;
    }

    /**
     * Sets properties of {@link Product}
     * @see ProductProperty
     *
     * @param properties - list of {@link ProductProperty}
     */
    public void setProperties(List<ProductProperty> properties) {
        this.properties = properties;
    }

    /**
     * Gets company producer of {@link Product}
     * @see Company
     *
     * @return company producer of {@link Product}
     */
    public Company getCompany() {
        return company;
    }

    /**
     * Sets company producer of {@link Customer}
     * @see Company
     *
     * @param company - {@link Customer}s company producer
     */
    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", properties=" + properties +
                ", company=" + company +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != null ? !id.equals(product.id) : product.id != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        return company != null ? company.equals(product.company) : product.company == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (company != null ? company.hashCode() : 0);
        return result;
    }
}
