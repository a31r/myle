package com.myle.core.model.validator;

import com.myle.core.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.myle.core.model.Product;

/**
 * Simple validator for {@link Product}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class ProductValidator implements Validator {

    @Autowired
    private ProductService productService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Product.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {

        Product product = (Product) o;
        String name = product.getName();

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "registrationForm.required");

    }
}
