package com.myle.core.model;

import com.myle.core.model.enums.CompanyPropertyType;

import javax.persistence.*;

/**
 * Implementation of {@link BasicProperty} interface for {@link Company}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "properties_of_companies")
public class CompanyProperty extends BasicProperty<CompanyPropertyType> {

    /**
     * Default constructor
     */
    public CompanyProperty() {
    }

    /**
     * Constructor with type and value
     *
     * @param type - type of property of {@link Company}
     *             @see CompanyPropertyType
     * @param value - value of property of {@link Company}
     */
    public CompanyProperty(CompanyPropertyType type, String value) {
        this.setType(type);
        this.setValue(value);
    }

}
