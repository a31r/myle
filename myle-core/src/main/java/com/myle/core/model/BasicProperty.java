package com.myle.core.model;


import javax.persistence.*;
import java.io.Serializable;

/**
 * Interface for properties
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@MappedSuperclass
public abstract class BasicProperty<Enum> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column
    private Enum type;

    @Column
    private String value;

    /**
     * Gets type of property
     *
     * @return type of property as {@link Enum}
     */
    Enum getType() {
        return this.type;
    }

    /**
     * Sets type of property
     *
     * @param type - property type as {@link Enum}
     */
    void setType(Enum type) {
        this.type = type;
    }

    /** Gets value of property
     *
     * @return value of property
     */
    String getValue() {
        return this.value;
    }

    /**
     * Sets value of property
     *
     * @param value - property value
     */
    void setValue(String value) {
        this.value = value;
    }

}
