package com.myle.core.model.enums;

import com.myle.core.model.Product;

/**
 * Simple enum for representing of types of properties of {@link Product}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public enum ProductPropertyType {
    PHONE
}
