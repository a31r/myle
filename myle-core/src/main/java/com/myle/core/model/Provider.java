package com.myle.core.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity of a {@link Provider}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "providers")
public class Provider implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    /**
     * Default constructor
     */
    public Provider() {
    }

    /**
     * Constructor with params
     *
     * @param product - provided {@link Product}
     */
    public Provider(Product product) {
        this.product = product;
    }

    /**
     * Gets id of {@link Provider}
     *
     * @return id of {@link Provider}
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id of {@link Provider}
     *
     * @param id - {@link Provider}s id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name of {@link Provider}
     *
     * @return name of {@link Provider}
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of {@link Provider}
     *
     * @param name - {@link Provider}s name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets product that {@link Provider} provides
     * @see Product
     *
     * @return - {@link Product}
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Sets product that {@link Provider} provides
     * @see Product
     *
     * @param product - product that {@link Provider} provides
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Provider{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", product=" + product +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Provider provider = (Provider) o;

        if (id != null ? !id.equals(provider.id) : provider.id != null) return false;
        if (name != null ? !name.equals(provider.name) : provider.name != null) return false;
        return product != null ? product.equals(provider.product) : provider.product == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (product != null ? product.hashCode() : 0);
        return result;
    }
}
