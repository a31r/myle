package com.myle.core.model.enums;

import com.myle.core.model.Company;

/**
 * Simple enum for representing of types of properties of {@link Company}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public enum CompanyPropertyType {
    ADDRESS, PHONE, EMAIL
}
