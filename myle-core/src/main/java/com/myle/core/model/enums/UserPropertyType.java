package com.myle.core.model.enums;

import com.myle.core.model.User;

/**
 * Simple enum for representing of types of properties of {@link User}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public enum UserPropertyType {
    ADDRESS, PHONE, EMAIL
}
