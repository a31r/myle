package com.myle.core.model;

import com.myle.core.model.enums.UserPropertyType;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Implementation of {@link BasicProperty} interface for {@link User}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Entity
@Table(name = "properties_of_users")
public class UserProperty extends BasicProperty<UserPropertyType> {

    /**
     * Default constructor
     */
    public UserProperty() {
    }

    /**
     * Constructor with type and value
     *
     * @param type - type of property of {@link User}
     *             @see UserPropertyType
     * @param value - value of property of {@link User}
     */
    public UserProperty(UserPropertyType type, String value) {
        this.setType(type);
        this.setValue(value);
    }
}
