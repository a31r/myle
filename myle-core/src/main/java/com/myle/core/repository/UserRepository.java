package com.myle.core.repository;

import com.myle.core.model.User;
import org.springframework.stereotype.Repository;

/**
 * Repository for {@link User} entity
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Repository
public interface UserRepository extends BasicRepository<User> {

    /**
     * Get {@link User} by username
     * @param username - username of {@link User}
     *
     * @return user {@link User}
     */
    User findByUsername(String username);

    /**
     * Get {@link User} by username
     * @param phone - phone of {@link User}
     *
     * @return user {@link User}
     */
    User findByPhone(String phone);

}
