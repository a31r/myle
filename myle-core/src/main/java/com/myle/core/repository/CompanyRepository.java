package com.myle.core.repository;

import com.myle.core.model.Company;

/**
 * Simple repository for {@link Company}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface CompanyRepository extends BasicRepository<Company> {
}
