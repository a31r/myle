package com.myle.core.repository;

import com.myle.core.model.Role;
import org.springframework.stereotype.Repository;

/**
 * Simple repository for {@link Role}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Repository
public interface RoleRepository extends BasicRepository<Role>{
}
