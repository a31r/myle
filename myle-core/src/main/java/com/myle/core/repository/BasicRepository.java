package com.myle.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Simple basic repository
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface BasicRepository<T> extends JpaRepository<T, Long> {
}
