package com.myle.core.repository;

import com.myle.core.model.Product;
import com.myle.core.model.Provider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Simple repository for {@link Provider}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Repository
public interface ProviderRepository extends BasicRepository<Provider> {

    List<Provider> findByProduct(Product product);

}
