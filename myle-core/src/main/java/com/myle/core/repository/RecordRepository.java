package com.myle.core.repository;

import com.myle.core.model.Customer;
import com.myle.core.model.Provider;
import com.myle.core.model.Record;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Simple repository for {@link Record}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Repository
public interface RecordRepository extends BasicRepository<Record> {

    List<Record> findByProvider(Provider provider);
    List<Record> findByCustomer(Customer customer);
    List<Record> findByProviderAndCustomer(Provider provider, Customer customer);
    List<Record> findByProviderIn(List<Provider> providers);
    List<Record> findByCustomerIn(List<Customer> customers);
    List<Record> findByProviderInAndCustomer(List<Provider> providers, Customer customer);

}
