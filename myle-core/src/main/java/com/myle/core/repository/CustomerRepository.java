package com.myle.core.repository;

import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Simple repository for {@link Customer}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Repository
public interface CustomerRepository extends BasicRepository<Customer> {

    List<Customer> findByUser(User user);
    List<Customer> findBySupplier(Company supplier);
    Customer findBySupplierAndUser(Company supplier, User user);
}
