package com.myle.core.repository;

import com.myle.core.model.Company;
import com.myle.core.model.Product;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Simple repository for {@link Product}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Repository
public interface ProductRepository extends BasicRepository<Product> {

    List<Product> findByCompany(Company company);

}
