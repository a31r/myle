package com.myle.core.repository;

import com.myle.core.model.Company;
import com.myle.core.model.Employee;
import com.myle.core.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Simple repository for {@link Employee}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Repository
public interface EmployeeRepository extends BasicRepository<Employee> {

    List<Employee> findByUser(User user);
    List<Employee> findByEmployer(Company employer);
    Employee findByEmployerAndUser(Company employer, User user);
}
