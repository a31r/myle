package com.myle.core.service.impl;

import com.myle.core.model.Company;
import com.myle.core.model.Product;
import com.myle.core.repository.ProductRepository;
import com.myle.core.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of {@link ProductService} interface
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Service
public class ProductServiceImpl extends BasicServiceImpl<Product> implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    /**
     * Creates {@link Product} for {@link Company}
     *
     * @param company - producer {@link Company}
     * @param product - {@link Product}
     * @return created {@link Product}
     */
    @Override
    public Product addProductToCompany(Company company, Product product) {
        product.setCompany(company);
        return add(product);
    }

    /**
     * Removes {@link Product} for {@link Company}
     *
     * @param company - producer {@link Company}
     * @param product - {@link Product}
     */
    @Override
    public void deleteProductOfCompany(Company company, Product product) {
        if (product.getCompany().equals(company))
            delete(product);
    }

    /**
     * Gets {@link Product}s of {@link Company}
     *
     * @param company - producer {@link Company}
     * @return - set of {@link Product}s
     */
    @Override
    public Set<Product> getProductsOfCompany(Company company) {
        return new HashSet<>(productRepository.findByCompany(company));
    }


}
