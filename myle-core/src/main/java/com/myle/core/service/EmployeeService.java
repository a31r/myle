package com.myle.core.service;

import com.myle.core.model.Company;
import com.myle.core.model.Employee;
import com.myle.core.model.User;

import java.util.Set;

/**
 * Interface of simple service for manipulating {@link Employee}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface EmployeeService extends BasicService<Employee> {

    /**
     * Check is {@link User} a {@link Employee} of {@link Company}
     *
     * @param company - supplier {@link Employee}
     * @param user - {@link User} for check
     * @return {@link Boolean} result of check
     */
    Boolean isEmployeeOfCompany(Company company, User user);

    /**
     * Gets {@link Employee} of {@link User} in {@link Company}
     *
     * @param company - {@link Company} in which {@link User} is {@link Employee}
     * @param user - {@link User} who is {@link Employee} of {@link Company}
     * @return {@link Employee} if exists or Null
     */
    Employee getEmployeeOfCompany(Company company, User user);

    /**
     * Creates new {@link Employee} of {@link Company}
     *
     * @param company - employee {@link Company}
     * @param user - registered {@link User}
     * @return - saved {@link Employee}
     */
    Employee addEmployeeToCompany(Company company, User user);

    /**
     * Removes {@link Employee} of {@link Company}
     *
     * @param company - employee {@link Company}
     * @param user - registered {@link User}
     */
    void deleteEmployeeOfCompany(Company company, User user);

    /**
     * Gets set of {@link Employee}s of {@link Company}
     *
     * @param company - employee {@link Company}
     * @return - set of {@link Employee}s
     */
    Set<Employee> getEmployeesOfCompany(Company company);

    /**
     * Gets set of {@link Employee}s of {@link User}
     *
     * @param user - registered {@link User}
     * @return - set of {@link Employee}s
     */
    Set<Employee> getEmployeeProfilesOfUser(User user);

}
