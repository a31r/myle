package com.myle.core.service.impl;

import com.myle.core.model.User;
import com.myle.core.repository.UserRepository;
import com.myle.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


/**
 * Implementation of {@link UserService} interface
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Service
public class UserServiceImpl extends BasicServiceImpl<User> implements UserService {

    @Autowired
    private UserRepository userRepository;

    /**
     * Get {@link User} by his phone number
     *
     * @param phone - {@link User}s phone number
     * @return {@link User}
     */
    @Override
    public User getUserByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }

    /**
     * Get {@link User} by his phone number
     *
     * @param username - {@link User}s username
     * @return {@link User}
     */
    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Gets authenticated {@link User}
     *
     * @return {@link User}
     */
    @Override
    public User getAuthenticatedUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user;

        if (authentication == null)
            throw new SecurityException("Could not get authenticated user!");

        if (authentication.getPrincipal() instanceof User)
            user = (User) authentication.getPrincipal();
        else if (authentication.getDetails() instanceof User)
            user = (User) authentication.getDetails();
        else {
            user = userRepository.findByUsername(authentication.getName());
        }

        return user;
    }

    /**
     * Implementation of loadUserByUsername() method of {@link UserDetailsService} interface
     *
     * @param username - {@link User}s username
     * @return {@link UserDetails}
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        // Add authorities for user from his roles
        user.getRoles().stream().forEach((role) -> {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        });

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }

}
