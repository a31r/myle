package com.myle.core.service.impl;

import com.myle.core.repository.BasicRepository;
import com.myle.core.service.BasicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class that implements {@link BasicService}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class BasicServiceImpl<T> implements BasicService<T> {

    @Autowired
    private BasicRepository<T> basicRepository;


    /**
     * Save new {@link T} to database
     *
     * @param entity - {@link T} for save
     * @return {@link T}
     */
    @Override
    @Transactional
    public T add(T entity) {
        return basicRepository.saveAndFlush(entity);
    }

    /**
     * Save changes to existing {@link T}
     *
     * @param entity - {@link T} for save
     * @return {@link T}
     */
    @Override
    @Transactional
    public T edit(T entity) {
        return basicRepository.saveAndFlush(entity);
    }

    /**
     * Delete {@link T} from database
     *
     * @param id - {@link T}s id for delete
     */
    @Override
    @Transactional
    public void delete(Long id) {
        basicRepository.delete(id);
    }

    /**
     * Delete {@link T} from database
     *
     * @param entity - {@link T} for delete
     */
    @Override
    @Transactional
    public void delete(T entity) {
        basicRepository.delete(entity);
    }

    /**
     * Get {@link T} by its id
     *
     * @param id - {@link T}s id
     * @return {@link T}
     */
    @Override
    @Transactional(readOnly = true)
    public T getById(Long id) {
        return basicRepository.findOne(id);
    }
}
