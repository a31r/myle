package com.myle.core.service.impl;

import com.myle.core.model.Product;
import com.myle.core.model.Provider;
import com.myle.core.repository.ProviderRepository;
import com.myle.core.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of {@link ProviderService} interface
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Service
public class ProviderServiceImpl extends BasicServiceImpl<Provider> implements ProviderService {

    @Autowired
    private ProviderRepository providerRepository;

    /**
     * Creates {@link Provider} for {@link Product}
     *
     * @param product  - provided {@link Product}
     * @param provider - {@link Provider}
     * @return - {@link Provider}
     */
    @Override
    public Provider addProviderToProduct(Product product, Provider provider) {
        provider.setProduct(product);
        return add(provider);
    }

    /**
     * Removes {@link Provider} for {@link Product}
     *
     * @param product  - provided {@link Product}
     * @param provider - {@link Provider}
     */
    @Override
    public void deleteProviderOfProduct(Product product, Provider provider) {
        if (provider.getProduct().equals(product))
            delete(provider);
    }

    /**
     * Gets {@link Provider}s of {@link Product}
     *
     * @param product - provided {@link Product}
     * @return - set of {@link Provider}s
     */
    @Override
    public Set<Provider> getProvidersOfProduct(Product product) {
        return new HashSet<>(providerRepository.findByProduct(product));
    }


}
