package com.myle.core.service.impl;

import com.myle.core.model.*;
import com.myle.core.repository.RecordRepository;
import com.myle.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of {@link RecordService} interface
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Service
public class RecordServiceImpl extends BasicServiceImpl<Record> implements RecordService {

    @Autowired
    private RecordRepository recordRepository;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProviderService providerService;
    @Autowired
    private UserService userService;
    @Autowired
    private CustomerService customerService;


    /**
     * Creates {@link Record} for {@link Customer} in {@link Provider}s queue
     *
     * @param provider - supplier. {@link Provider}
     * @param record   - {@link Record}
     * @return - {@link Record}
     */
    @Override
    public Record addRecordToProvider(Provider provider, Record record) {

        record.setProvider(provider);
        return add(record);

    }

    /**
     * Creates {@link Record} for {@link Customer} in {@link Provider}s queue
     *
     * @param provider - supplier. {@link Provider}
     * @param record   - {@link Record}
     * @return - {@link Record}
     */
    @Override
    public void deleteRecordOfProvider(Provider provider, Record record) {

        if (record.getProvider().equals(provider))
            delete(record);

    }

    /**
     * Gets set of {@link Record}s of {@link User} in services of a {@link Company}
     *
     * @param company - {@link Company} in which {@link User} have {@link Record}s
     * @param user    - {@link User} who have {@link Record}s
     * @return - set of {@link Record}s
     */
    @Override
    public Set<Record> getRecordsOfUserInCompany(Company company, User user) {

        Set<Record> records = new HashSet<>();

        Customer customer = customerService.getCustomerOfCompany(company, user);

        if (customer != null)
            records.addAll(recordRepository.findByCustomer(customer));

        return records;

    }

    /**
     * Gets set of {@link Record}s of {@link User} in all services
     *
     * @param user    - {@link User} who have {@link Record}s
     * @return - set of {@link Record}s
     */
    @Override
    public Set<Record> getRecordsOfUser(User user) {

        Set<Record> records = new HashSet<>();

        List<Customer> customers = new ArrayList<>(customerService.getCustomersOfUser(user));

        if (!customers.isEmpty())
            records.addAll(recordRepository.findByCustomerIn(customers));

        return records;

    }

    /**
     * Gets set of {@link Record}s of {@link User} in services of a {@link Company}
     *
     * @param product - {@link Product} in which {@link User} have {@link Record}s
     * @param user    - {@link User} who have {@link Record}s
     * @return - set of {@link Record}s
     */
    @Override
    public Set<Record> getRecordsOfUserInProduct(Product product, User user) {

        Set<Record> records = new HashSet<>();

        Customer customer = customerService
                .getCustomerOfCompany(product.getCompany(), user);

        if (customer == null)
            return records;

        List<Provider> providers = new ArrayList<>(providerService.getProvidersOfProduct(product));

        if (!providers.isEmpty())
            records.addAll(recordRepository.findByProviderInAndCustomer(providers, customer));

        return records;

    }

    /**
     * Gets set of {@link Record}s of {@link User} in services of a {@link Company}
     *
     * @param provider - {@link Provider} in which {@link User} have {@link Record}s
     * @param user     - {@link User} who have {@link Record}s
     * @return - set of {@link Record}s
     */
    @Override
    public Set<Record> getRecordsOfUserInProvider(final Provider provider, final User user) {

        Set<Record> records = new HashSet<>();

        final Company company = provider.getProduct().getCompany();
        final Customer customer = customerService.getCustomerOfCompany(company, user);

        if (company != null && customer != null)
            records.addAll(recordRepository.findByProviderAndCustomer(provider, customer));

        return records;

    }

    /**
     * Gets set of {@link Record}s of {@link Company}
     *
     * @param company - {@link Company}
     * @return - set of {@link Record}s
     */
    @Override
    public Set<Record> getRecordsOfCompany(final Company company) {

        Set<Record> records = new HashSet<>();
        List<Provider> providers = new ArrayList<>();

        productService.getProductsOfCompany(company).forEach(product -> {
            providers.addAll(providerService.getProvidersOfProduct(product));
        });

        if (!providers.isEmpty())
            records.addAll(recordRepository.findByProviderIn(providers));

        return records;

    }

    /**
     * Gets set of {@link Record}s of {@link Product}
     *
     * @param product - {@link Product}
     * @return - set of {@link Record}s
     */
    @Override
    public Set<Record> getRecordsOfProduct(final Product product) {

        Set<Record> records = new HashSet<>();

        final List<Provider> providers = new ArrayList<>(providerService.getProvidersOfProduct(product));

        if (!providers.isEmpty())
            records.addAll(recordRepository.findByProviderIn(providers));

        return records;

    }

    /**
     * Gets set of {@link Record}s of {@link Provider}
     *
     * @param provider - {@link Provider}
     * @return - set of {@link Record}s
     */
    @Override
    public Set<Record> getRecordsOfProvider(Provider provider) {

        return new HashSet<>(recordRepository.findByProvider(provider));

    }


}
