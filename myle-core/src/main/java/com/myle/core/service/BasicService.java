package com.myle.core.service;

/**
 * Basic interface of service for CRUD operations
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface BasicService<T> {

    /**
     * Save new {@link T} to database
     *
     * @param entity - {@link T} for save
     * @return {@link T}
     */
    T add(T entity);

    /**
     * Save changes to existing {@link T}
     *
     * @param entity - {@link T} for save
     * @return {@link T}
     */
    T edit(T entity);

    /**
     * Delete {@link T} from database
     *
     * @param id - {@link T}s id for delete
     */
    void delete(Long id);

    /**
     * Delete {@link T} from database
     *
     * @param entity - {@link T} for delete
     */
    void delete(T entity);

    /**
     * Get {@link T} by its id
     * @param id - {@link T}s id
     * @return {@link T}
     */
    T getById(Long id);

}
