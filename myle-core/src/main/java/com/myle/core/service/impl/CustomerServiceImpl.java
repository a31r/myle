package com.myle.core.service.impl;

import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.User;
import com.myle.core.repository.CustomerRepository;
import com.myle.core.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of {@link CustomerService} interface
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Service
public class CustomerServiceImpl extends BasicServiceImpl<Customer> implements CustomerService {


    @Autowired
    private CustomerRepository customerRepository;

    /**
     * Check is {@link User} a {@link Customer} of {@link Company}
     *
     * @param company - supplier {@link Customer}
     * @param user    - {@link User} for check
     * @return {@link Boolean} result of check
     */
    @Override
    public Boolean isCustomerOfCompany(Company company, User user) {
        Customer customer = getCustomerOfCompany(company, user);
        return customer != null;
    }

    /**
     * Gets {@link Customer} of {@link User} in {@link Company}
     *
     * @param company - {@link Company} in which {@link User} is {@link Customer}
     * @param user    - {@link User} who is {@link Customer} of {@link Company}
     * @return {@link Company} if exists or Null
     */
    @Override
    public Customer getCustomerOfCompany(Company company, User user) {
        return customerRepository.findBySupplierAndUser(company, user);
    }

    /**
     * Creates and saves new {@link Customer} of {@link Company}
     *
     * @param company - consumer {@link Company}
     * @param user    - registered {@link User}
     * @return - saved {@link Customer}
     */
    @Override
    @Transactional
    public Customer addCustomerToCompany(Company company, User user) {
        Customer customer = new Customer(company, user);
        return add(customer);
    }

    /**
     * Removes {@link Customer} of {@link Company}
     *
     * @param company - consumer {@link Company}
     * @param user    - registered {@link User}
     */
    @Override
    @Transactional
    public void deleteCustomerOfCompany(Company company, User user) {
        Customer customer = getCustomerOfCompany(company, user);
        if (customer != null)
            customerRepository.delete(customer);
    }

    /**
     * Gets set of {@link Customer}s of {@link Company}
     *
     * @param company - consumer {@link Company}
     * @return - set of {@link Customer} or Null
     */
    @Override
    public Set<Customer> getCustomersOfCompany(Company company) {
        return new HashSet<>(customerRepository.findBySupplier(company));
    }

    /**
     * Gets set of {@link Customer}s of {@link User}
     *
     * @param user - registered {@link User}
     * @return - set of {@link Customer} or Null
     */
    @Override
    public Set<Customer> getCustomersOfUser(User user) {
        return new HashSet<>(customerRepository.findByUser(user));
    }


}
