package com.myle.core.service;

import com.myle.core.model.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Interface of simple service for manipulating {@link Record}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface RecordService extends BasicService<Record> {

    /**
     * Creates {@link Record} for {@link Customer} in {@link Provider}s queue
     *
     * @param provider - supplier. {@link Provider}
     * @param record - {@link Record}
     * @return - {@link Record}
     */
    Record addRecordToProvider(Provider provider, Record record);

    /**
     * Creates {@link Record} for {@link Customer} in {@link Provider}s queue
     *
     * @param provider - supplier. {@link Provider}
     * @param record - {@link Record}
     * @return - {@link Record}
     */
    void deleteRecordOfProvider(Provider provider, Record record);

    /**
     * Gets set of {@link Record}s of {@link User} in services of a {@link Company}
     *
     * @param company - {@link Company} in which {@link User} have {@link Record}s
     * @param user - {@link User} who have {@link Record}s
     * @return - set of {@link Record}s
     */
    Set<Record> getRecordsOfUserInCompany(Company company, User user);

    /**
     * Gets set of {@link Record}s of {@link User} in all services
     *
     * @param user - {@link User} who have {@link Record}s
     * @return - set of {@link Record}s
     */
    Set<Record> getRecordsOfUser(User user);

    /**
     * Gets set of {@link Record}s of {@link User} in services of a {@link Company}
     *
     * @param product - {@link Product} in which {@link User} have {@link Record}s
     * @param user - {@link User} who have {@link Record}s
     * @return - set of {@link Record}s
     */
    Set<Record> getRecordsOfUserInProduct(Product product, User user);

    /**
     * Gets set of {@link Record}s of {@link User} in services of a {@link Company}
     *
     * @param provider - {@link Provider} in which {@link User} have {@link Record}s
     * @param user - {@link User} who have {@link Record}s
     * @return - set of {@link Record}s
     */
    Set<Record> getRecordsOfUserInProvider(Provider provider, User user);

    /**
     * Gets set of {@link Record}s of {@link Company}
     *
     * @param company - {@link Company}
     * @return - set of {@link Record}s
     */
    Set<Record> getRecordsOfCompany(Company company);

    /**
     * Gets set of {@link Record}s of {@link Product}
     *
     * @param product - {@link Product}
     * @return - set of {@link Record}s
     */
    Set<Record> getRecordsOfProduct(Product product);

    /**
     * Gets set of {@link Record}s of {@link Provider}
     *
     * @param provider - {@link Provider}
     * @return - set of {@link Record}s
     */
    Set<Record> getRecordsOfProvider(Provider provider);

}
