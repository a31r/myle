package com.myle.core.service;

import com.myle.core.model.Product;
import com.myle.core.model.Provider;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Interface of simple service for manipulating {@link Provider}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface ProviderService extends BasicService<Provider> {

    /**
     * Creates {@link Provider} for {@link Product}
     *
     * @param product - provided {@link Product}
     * @param provider - {@link Provider}
     * @return - {@link Provider}
     */
    Provider addProviderToProduct(Product product, Provider provider);

    /**
     * Removes {@link Provider} for {@link Product}
     *
     * @param product - provided {@link Product}
     * @param provider - {@link Provider}
     */
    void deleteProviderOfProduct(Product product, Provider provider);

    /**
     * Gets {@link Provider}s of {@link Product}
     *
     * @param product - provided {@link Product}
     * @return - set of {@link Provider}s
     */
    Set<Provider> getProvidersOfProduct(Product product);

}
