package com.myle.core.service.impl;

import com.myle.core.model.Company;
import com.myle.core.model.Employee;
import com.myle.core.model.User;
import com.myle.core.repository.EmployeeRepository;
import com.myle.core.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of {@link EmployeeService} interface
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Service
public class EmployeeServiceImpl extends BasicServiceImpl<Employee> implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Check is {@link User} a {@link Employee} of {@link Company}
     *
     * @param company - supplier {@link Employee}
     * @param user    - {@link User} for check
     * @return {@link Boolean} result of check
     */
    @Override
    public Boolean isEmployeeOfCompany(Company company, User user) {
        return getEmployeeOfCompany(company, user) != null;
    }

    /**
     * Gets {@link Employee} of {@link User} in {@link Company}
     *
     * @param company - {@link Company} in which {@link User} is {@link Employee}
     * @param user    - {@link User} who is {@link Employee} of {@link Company}
     * @return {@link Employee} if exists or Null
     */
    @Override
    public Employee getEmployeeOfCompany(Company company, User user) {
        return employeeRepository.findByEmployerAndUser(company, user);
    }

    /**
     * Creates new {@link Employee} of {@link Company}
     *
     * @param company - employee {@link Company}
     * @param user    - registered {@link User}
     * @return - saved {@link Employee}
     */
    @Override
    @Transactional
    public Employee addEmployeeToCompany(Company company, User user) {
        Employee employee = new Employee(company, user);
        return add(employee);
    }

    /**
     * Removes {@link Employee} of {@link Company}
     *
     * @param company - employee {@link Company}
     * @param user    - registered {@link User}
     */
    @Override
    @Transactional
    public void deleteEmployeeOfCompany(Company company, User user) {
        Employee employee = getEmployeeOfCompany(company, user);
        if (employee != null)
            delete(employee);
    }

    /**
     * Gets set of {@link Employee}s of {@link Company}
     *
     * @param company - employee {@link Company}
     * @return - set of {@link Employee}s
     */
    @Override
    public Set<Employee> getEmployeesOfCompany(Company company) {
        return new HashSet<>(employeeRepository.findByEmployer(company));
    }

    /**
     * Gets set of {@link Employee}s of {@link User}
     *
     * @param user - registered {@link User}
     * @return - set of {@link Employee}s
     */
    @Override
    public Set<Employee> getEmployeeProfilesOfUser(User user) {
        return new HashSet<>(employeeRepository.findByUser(user));
    }


}
