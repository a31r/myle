package com.myle.core.service;

import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.Employee;
import com.myle.core.model.User;

import java.util.Set;

/**
 * Interface of simple service for manipulating {@link Company}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface CompanyService extends BasicService<Company> {

    /**
     * Gets set of {@link Company}s in which {@link User} is {@link Customer}
     *
     * @param user - {@link User} for whom get company suppliers
     * @return - set of {@link Company}
     */
    Set<Company> getSupplierCompanies(User user);

    /**
     * Gets set of {@link Company}s in which {@link User} is {@link Employee}
     *
     * @param user - {@link User} for whom get company suppliers
     * @return - set of {@link Company}
     */
    Set<Company> getEmployerCompanies(User user);

    /**
     * Adds to base new {@link Company}
     *
     * @param company - new {@link Company}
     * @param user - owner of {@link Company}
     * @return - saved {@link Company}
     */
    Company registerUsersCompany(Company company, User user);

    /**
     * Delete {@link Company} of {@link User}
     *
     * @param company - company which will deleted
     * @param user - owner of company
     */
    void deleteUsersCompany(Company company, User user);

    /**
     * Gets set of created {@link Company}s of {@link User}
     *
     * @param user - owner of companies
     * @return - set of {@link Company}s
     */
    Set<Company> getUsersCompanies(User user);

}
