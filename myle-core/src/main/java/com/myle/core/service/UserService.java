package com.myle.core.service;

import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.Employee;
import com.myle.core.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;


/**
 * Simple service for manipulating {@link User}s
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface UserService extends BasicService<User>, UserDetailsService{

    /**
     * Get {@link User} by his phone number
     *
     * @param phone - {@link User}s phone number
     * @return {@link User}
     */
    User getUserByPhone(String phone);

    /**
     * Get {@link User} by username
     *
     * @param username - {@link User}s username
     * @return {@link User}
     */
    User getUserByUsername(String username);

    /**
     * Gets authenticated {@link User}
     *
     * @return {@link User}
     */
    User getAuthenticatedUser();

}
