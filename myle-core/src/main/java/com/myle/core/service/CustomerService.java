package com.myle.core.service;

import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.User;

import java.util.Set;

/**
 * Interface of simple service for manipulating {@link Customer}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface CustomerService extends BasicService<Customer> {

    /**
     * Check is {@link User} a {@link Customer} of {@link Company}
     *
     * @param company - supplier {@link Customer}
     * @param user - {@link User} for check
     * @return {@link Boolean} result of check
     */
    Boolean isCustomerOfCompany(Company company, User user);

    /**
     * Gets {@link Customer} of {@link User} in {@link Company}
     *
     * @param company - {@link Company} in which {@link User} is {@link Customer}
     * @param user - {@link User} who is {@link Customer} of {@link Company}
     * @return {@link Customer} if exists or Null
     */
    Customer getCustomerOfCompany(Company company, User user);

    /**
     * Creates and saves new {@link Customer} of {@link Company}
     *
     * @param company - consumer {@link Company}
     * @param user - registered {@link User}
     * @return - saved {@link Customer}
     */
    Customer addCustomerToCompany(Company company, User user);

    /**
     * Removes {@link Customer} of {@link Company}
     *
     * @param company - consumer {@link Company}
     * @param user - registered {@link User}
     */
    void deleteCustomerOfCompany(Company company, User user);

    /**
     * Gets set of {@link Customer}s of {@link Company}
     *
     * @param company - consumer {@link Company}
     * @return - set of {@link Customer} or Null
     */
    Set<Customer> getCustomersOfCompany(Company company);

    /**
     * Gets set of {@link Customer}s of {@link User}
     *
     * @param user - registered {@link User}
     * @return - set of {@link Customer} or Null
     */
    Set<Customer> getCustomersOfUser(User user);

}
