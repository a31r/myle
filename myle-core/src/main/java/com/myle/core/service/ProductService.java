package com.myle.core.service;

import com.myle.core.model.Company;
import com.myle.core.model.Product;
import com.myle.core.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Interface of simple service for manipulating {@link Product}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public interface ProductService extends BasicService<Product> {

    /**
     * Creates {@link Product} for {@link Company}
     *
     * @param company - producer {@link Company}
     * @param product - {@link Product}
     * @return created {@link Product}
     */
    Product addProductToCompany(Company company, Product product);

    /**
     * Removes {@link Product} for {@link Company}
     *
     * @param company - producer {@link Company}
     * @param product - {@link Product}
     */
    void deleteProductOfCompany(Company company, Product product);

    /**
     * Gets {@link Product}s of {@link Company}
     *
     * @param company - producer {@link Company}
     * @return - set of {@link Product}s
     */
    Set<Product> getProductsOfCompany(Company company);

}
