package com.myle.core.service.impl;

import com.myle.core.model.Company;
import com.myle.core.model.Customer;
import com.myle.core.model.Employee;
import com.myle.core.model.User;
import com.myle.core.repository.CompanyRepository;
import com.myle.core.service.CompanyService;
import com.myle.core.service.CustomerService;
import com.myle.core.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of {@link CompanyService} interface
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Service
public class CompanyServiceImpl extends BasicServiceImpl<Company> implements CompanyService{

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private EmployeeService employeeService;

    /**
     * Gets set of {@link Company}s in which {@link User} is {@link Customer}
     *
     * @param user - {@link User} for whom get company suppliers
     * @return - set of {@link Company}
     */
    @Override
    public Set<Company> getSupplierCompanies(User user) {
        Set<Company> companies = new HashSet<>();
        customerService
                .getCustomersOfUser(user)
                .forEach(customer -> companies.add(customer.getSupplier()));
        return companies;
    }

    /**
     * Gets set of {@link Company}s in which {@link User} is {@link Employee}
     *
     * @param user - {@link User} for whom get company suppliers
     * @return - set of {@link Company}
     */
    @Override
    public Set<Company> getEmployerCompanies(User user) {
        Set<Company> companies = new HashSet<>();
        employeeService
                .getEmployeeProfilesOfUser(user)
                .forEach(customer -> companies.add(customer.getEmployer()));
        return companies;
    }

    /**
     * Adds to base new {@link Company}
     *
     * @param company - new {@link Company}
     * @param user    - owner of {@link Company}
     * @return - saved {@link Company}
     */
    @Override
    @Transactional
    public Company registerUsersCompany(Company company, User user) {
        company = add(company);
        employeeService.addEmployeeToCompany(company, user);
        return company;
    }

    /**
     * Delete {@link Company} of {@link User}
     *
     * @param company - company which will deleted
     * @param user    - owner of company
     */
    @Override
    @Transactional
    public void deleteUsersCompany(Company company, User user) {
        if (getUsersCompanies(user).contains(company)) {
            employeeService.deleteEmployeeOfCompany(company, user);
            delete(company);
        }
    }

    /**
     * Gets set of created {@link Company}s of {@link User}
     *
     * @param user - owner of companies
     * @return - set of {@link Company}s
     */
    @Override
    public Set<Company> getUsersCompanies(User user) {
        Set<Company> companies = new HashSet<>();
        employeeService
                .getEmployeeProfilesOfUser(user)
                .forEach(employee -> companies.add(employee.getEmployer()));
        return companies;
    }


}
