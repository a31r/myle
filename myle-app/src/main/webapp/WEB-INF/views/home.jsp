<%--
    Document   : home
    Created on : 11.09.2016, 18:13:20
    Author     : Админ
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="<c:url value="/static/css/bootstrap.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/static/css/narrow-jumbotron.css"/>"/>
        <script type="text/javascript" src="<c:url value="/static/js/main.js"/>" charset="UTF-8"></script>
        <title>Home Page</title>
    </head>
    <body>
        <%
            if (request.getSession() != null) {
                if (request.getUserPrincipal() != null)
                    response.sendRedirect("/app/profile/");
            }
        %>
        <div class="container">
            <div class="header clearfix">
                <nav>
                    <ul class="nav nav-pills pull-xs-right">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                    </ul>
                </nav>
                <h3 class="text-muted">MYLE</h3>
            </div>

            <div class="jumbotron">
                <h1 class="display-3">Добро пожаловать в MYLE</h1>
                <p class="lead">Сделайте вашу жизнь проще</p>
                <p>
                    <a class="btn btn-lg btn-success" href="<c:url value="/registration"/>" role="button">Зарегистрироваться</a>
                    <strong>или</strong>
                    <a class="btn btn-lg btn-success" href="<c:url value="/login"/>" role="button">Войти</a>
                </p>
            </div>
        </div>
    </body>
</html>
