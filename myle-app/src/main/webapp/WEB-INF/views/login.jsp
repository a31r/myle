<%--
    Document   : login
    Created on : 11.09.2016, 21:05:33
    Author     : Админ
--%>

<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="<c:url value="/static/css/bootstrap.css"/>"/>
        <script href="<c:url value="/static/js/bootstrap.js"/> "/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <title>Login</title>
    </head>
    <body>
    <div class="container">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info" >
                <div class="panel-heading">
                    <div class="panel-title">Войти</div>
                    <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Забыли пароль?</a></div>
                </div>

                <div style="padding-top:30px" class="panel-body" >

                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                    <sf:form action="/app/login" method="post" cssClass="form-horizontal" id="loginform" role="form">

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="логин или телефон"/>
                        </div>

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input id="login-password" type="password" class="form-control" name="password" placeholder="пароль">
                        </div>
                        <c:if test="${error != null}">
                            <div class="alert alert-danger">
                                ${error}
                            </div>
                        </c:if>
                        <div class="input-group">
                            <div class="checkbox">
                                <label>
                                    <input id="login-remember" type="checkbox" name="remember" value="1"> Запомнить меня
                                </label>
                            </div>
                        </div>

                        <div style="margin-top:10px" class="form-group">
                            <!-- Button -->

                            <div class="col-sm-12 controls">
                                <input type="submit" id="btn-login" class="btn btn-success" value="Войти "/>
                                <a href="/app/registration" class="btn btn-info">Зарегистрироваться</a>
                            </div>
                        </div>
                    </sf:form>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
