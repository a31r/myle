<%-- 
    Document   : registration
    Created on : 11.09.2016, 20:50:45
    Author     : Админ
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="<c:url value="/static/css/bootstrap.css"/>"/>
        <%--<script href="<c:url value="/static/js/bootstrap.js"/> "/>--%>
        <title>Sign up</title>
    </head>
    <body>
    <div class="container">
        <div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Регистрация</div>
                    <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="<c:url value="/login"/>" >Войти</a></div>
                </div>
                <div class="panel-body" >
                    <sf:form commandName="user" method="post" id="signupform" class="form-horizontal" role="form">

                        <div id="signupalert" style="display:none" class="alert alert-danger">
                            <p>Error:</p>
                            <span></span>
                        </div>

                        <div class="form-group">
                            <sf:label path="username" cssClass="col-md-3 control-label">Логин</sf:label>
                            <div class="col-sm-9">
                                <sf:input path="username" cssClass="form-control" placeholder="Выберите логин"/>
                                <sf:errors path="username" cssClass="help-block" lang="ru"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <sf:label path="phone" cssClass="col-sm-3 control-label">Телефон</sf:label>
                            <div class="col-sm-9">
                                <sf:input path="phone" type="tel" cssClass="form-control" placeholder="Телефон"/>
                                <sf:errors path="phone" cssClass="help-block" lang="ru"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <sf:label path="password" cssClass="col-sm-3 control-label">Пароль</sf:label>
                            <div class="col-sm-9">
                                <sf:password path="password" cssClass="form-control" placeholder="Пароль"/>
                                <sf:errors path="password" cssClass="help-block" lang="ru"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <!-- Button -->
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Зарегистрироваться</button>
                                    <%--<span style="margin-left:8px;">or</span>--%>
                            </div>
                        </div>
                    </sf:form>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
