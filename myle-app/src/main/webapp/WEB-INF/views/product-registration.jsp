<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Админ
  Date: 27.09.2016
  Time: 16:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add product</title>
    <link rel="stylesheet" href="<c:url value="/static/css/bootstrap.css"/>"/>
</head>
<body>
<div class="container">
    <div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Регистрация продукта компании</div>
            </div>
            <div class="panel-body" >
                <sf:form commandName="product" method="post" id="signupform" class="form-horizontal" role="form">

                    <div id="signupalert" style="display:none" class="alert alert-danger">
                        <p>Error:</p>
                        <span></span>
                    </div>

                    <div class="form-group">
                        <sf:label path="company" cssClass="col-md-3 control-label">Компания</sf:label>
                        <div class="col-sm-9">
                            <sf:input path="company" cssClass="form-control" placeholder="${company.name}" disabled="true"/>
                            <sf:errors path="company" cssClass="help-block" lang="ru"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <sf:label path="name" cssClass="col-md-3 control-label">Название</sf:label>
                        <div class="col-sm-9">
                            <sf:input path="name" cssClass="form-control" placeholder="Введите название продукта"/>
                            <sf:errors path="name" cssClass="help-block" lang="ru"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Добавить</button>
                                <%--<span style="margin-left:8px;">or</span>--%>
                        </div>
                    </div>
                </sf:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
