<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Админ
  Date: 26.09.2016
  Time: 17:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${company.name}</title>
    <link rel="stylesheet" href="<c:url value="/static/css/bootstrap.css"/>"/>
</head>
<body>
This is ${company.name} page!<p>
<div>
    Products:
    <div>
        <c:forEach items="${company.products}" var="product">
            <li id="product_<c:out value="product.id"/>">
                <div>
                    <c:out value="${product.name}"/>
                </div>
            </li>
        </c:forEach>
    </div>
<a href="<c:url value="/company/${company.id}/product/add"/>" class="btn btn-info">Добавить продукт</a>
</div>
</body>
</html>
