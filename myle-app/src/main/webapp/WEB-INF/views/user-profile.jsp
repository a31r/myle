<%--
  Created by IntelliJ IDEA.
  User: Админ
  Date: 20.09.2016
  Time: 1:13
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
    <h1>Your profile</h1>
    <a href="<c:url value="/logout"/>">Log out</a>
    <c:out value="${user.firstName}"/><br/>
    <c:out value="${user.lastName}"/><br/>
    <c:out value="${user.email}"/><br/>
    <c:out value="${user.username}"/><br/>
    <a href="<c:url value="/company/registration"/>" class="btn btn-info">Зарегистрировать компанию</a>
</body>
</html>
