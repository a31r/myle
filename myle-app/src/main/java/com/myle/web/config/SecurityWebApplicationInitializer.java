/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myle.web.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Web application security initializer class
 * 
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer{
    
}
