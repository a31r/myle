/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myle.web.config;

import com.myle.core.model.validator.CompanyValidator;
import com.myle.core.model.validator.ProductValidator;
import com.myle.core.model.validator.UserValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Root configuration for Spring MVC
 * 
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Configuration
@ComponentScan(basePackages = {"com.myle"},
        excludeFilters = {
            @ComponentScan.Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class)
        })
public class RootConfig {

    @Bean
    public UserValidator userValidator() {
        return new UserValidator();
    }

    @Bean
    public CompanyValidator companyValidator() {
        return new CompanyValidator();
    }

    @Bean
    public ProductValidator productValidator() {
        return new ProductValidator();
    }
}
