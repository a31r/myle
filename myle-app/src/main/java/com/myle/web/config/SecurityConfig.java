/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myle.web.config;

import com.myle.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Spring Security configuration
 * 
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception{

        http
                .formLogin()
                    .loginPage("/login")
                .and()
                .rememberMe()
                    .tokenValiditySeconds(600)
                    .key("userKey")
                .and()
                    .logout()
                    .logoutSuccessUrl("/")
                .and()
                .httpBasic()
                .and()
                .csrf()
                    .disable();

        http
                .authorizeRequests()
                    .antMatchers("/login").permitAll()
                    .antMatchers("/static/**").permitAll()
                    .antMatchers("/").permitAll()
                    .antMatchers("/registration").permitAll()
                    .anyRequest().authenticated();
    }

    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth
                .userDetailsService(userService);

    }
}
