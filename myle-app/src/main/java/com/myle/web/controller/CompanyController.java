package com.myle.web.controller;

import com.myle.core.model.Company;
import com.myle.core.model.Product;
import com.myle.core.model.validator.CompanyValidator;
import com.myle.core.model.validator.ProductValidator;
import com.myle.core.service.CompanyService;
import com.myle.core.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Controller for handling requests from user profile page
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Controller
@RequestMapping("/company")
public class CompanyController {

    private final CompanyService companyService;

    @Autowired
    private ProductService productService;

    @Autowired
    @Qualifier("companyValidator")
    private CompanyValidator companyValidator;

    @Autowired
    @Qualifier("productValidator")
    private ProductValidator productValidator;

    @InitBinder("company")
    private void initCompanyBinder(WebDataBinder binder) {
        binder.setValidator(companyValidator);
    }

    @InitBinder("product")
    private void initProductBinder(WebDataBinder binder) {
        binder.setValidator(productValidator);
    }

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @RequestMapping(value = "/registration", method = GET)
    public String showCompanyRegistrationPage(Model model) {

        model.addAttribute(new Company());

        return "company-registration";
    }

    @RequestMapping(value = "/registration", method = POST)
    public String processCompanyRegistration(@ModelAttribute("company") @Validated Company company, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "company-registration";
        }

        company = companyService.add(company);

        return "redirect:/"+company.getId()+"/profile";
    }

    @RequestMapping(value = "/{id}/profile", method = GET)
    public String showCompanyProfilePage(@PathVariable("id") Long id, Model model) {

        Company company = companyService.getById(id);
        model.addAttribute("company",company);

        return "company-profile";

    }

    @RequestMapping(value = "/{id}/product/add", method = GET)
    public String showCompanyProductRegistrationPage(@PathVariable("id") Long id, Model model) {

        Company company = companyService.getById(id);
        model.addAttribute("company",company);
        model.addAttribute("product", new Product());

        return "product-registration";

    }

    @RequestMapping(value = "/{id}/product/add", method = POST)
    public String processCompanyProductRegistration(@PathVariable("id") Long id,
                                                    @ModelAttribute("product") @Validated Product product,
                                                    BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()){
            return "product-registration";
        }

        Company company = companyService.getById(id);
        product.setCompany(company);
        company.getProducts().add(product);
        company = companyService.edit(company);

        model.addAttribute("company", company);

        return "redirect:/company/"+company.getId()+"/profile";

    }

}
