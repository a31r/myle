package com.myle.web.controller;

import com.myle.core.model.User;
import com.myle.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Controller for handling requests from user profile page
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Controller
@RequestMapping("/profile")
public class ProfileController {

    private final UserService userService;

    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/", method = GET)
    public String showUserProfile(Model model) {

        User user = userService.getAuthenticatedUser();
        model.addAttribute("user", user);

        return "user-profile";

    }

}
