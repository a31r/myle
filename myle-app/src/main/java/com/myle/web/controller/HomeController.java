/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myle.web.controller;

import com.myle.core.model.User;
import com.myle.core.model.validator.UserValidator;
import com.myle.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Controller for handling requests to home page
 * 
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@Controller
public class HomeController {
    
    private final UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    @Qualifier("userValidator")
    private UserValidator userValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(userValidator);
    }


    @RequestMapping(value = "/", method = GET)
    public String showHomePage() {
        return "home";
    }

    @RequestMapping(value = "/registration", method = GET)
    public String showRegistrationPage(Model model) {

        model.addAttribute(new User());

        return "user-registration";

    }
    
    @RequestMapping(value = "/registration", method = POST)
    public String processRegistration(@ModelAttribute("user") @Validated User user, BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
            return "user-registration";
        }
        
        userService.add(user);
        
        return "redirect:/profile/";

    }
    
    @RequestMapping(value = "/login", method = GET)
    public String showLoginPage(Model model, String error) {

        if(error != null) {
            model.addAttribute("error", "Неправильный логин или пароль");
        }

        model.addAttribute("user", new User());

        return "login";

    }

}
