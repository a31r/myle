package com.myle.web.controller.rest;

import com.myle.core.model.Company;
import com.myle.core.service.CompanyService;
import com.myle.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * REST controller for handling requests from user profile page
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@RestController
@RequestMapping("/profile")
public class RestProfileController {

    private final UserService userService;
    private final CompanyService companyService;

    @Autowired
    public RestProfileController(UserService userService, CompanyService companyService) {
        this.userService = userService;
        this.companyService = companyService;
    }

    @RequestMapping(value = "/suppliers/list", method = POST)
    public Set<Company> getUserSupplier() {

        return companyService.getSupplierCompanies(userService.getAuthenticatedUser());

    }

}
