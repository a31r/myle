package com.myle.web.util;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * Utility class for configuration test classes
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
public class ConfigUtil {

    /**
     * Verify that the breaking change introduced in <a
     * href="https://jira.spring.io/browse/SPR-12553">SPR-12553</a> has been reverted.
     *
     */
    public static void verifyRootWacSupport(WebApplicationContext wac, Object... objects) {

        for (Object o: objects) {
            assertNotNull(o);
        }

        ApplicationContext parent = wac.getParent();
        assertNotNull(parent);
        assertTrue(parent instanceof WebApplicationContext);
        WebApplicationContext root = (WebApplicationContext) parent;

        ServletContext childServletContext = wac.getServletContext();
        assertNotNull(childServletContext);
        ServletContext rootServletContext = root.getServletContext();
        assertNotNull(rootServletContext);
        assertSame(childServletContext, rootServletContext);

        assertSame(root, rootServletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE));
        assertSame(root, childServletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE));

    }

}
