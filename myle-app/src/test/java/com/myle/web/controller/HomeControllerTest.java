/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myle.web.controller;

import com.myle.core.model.User;
import com.myle.core.service.UserService;
import com.myle.web.config.WebConfig;
import com.myle.web.util.ConfigUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

/**
 * Test class for {@link HomeController}
 * 
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
        @ContextConfiguration(classes = HomeControllerTest.RootConfig.class),
        @ContextConfiguration(classes = WebConfig.class)
})
public class HomeControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserService userService;

    @Autowired
    private HomeController homeController;

    private MockMvc mockMvc;

    @Before
    public void setup() {

        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        ConfigUtil.verifyRootWacSupport(wac, userService, homeController);

    }

    @Configuration
    static class RootConfig {

        @Bean
        public UserService userService() {
            return mock(UserService.class);
        }
    }

    @Test
    public void testsShowHomePage() throws Exception {

        mockMvc
                .perform(get("/"))
                .andExpect(view().name("home"));

    }

    @Test
    public void shouldShowRegistrationPage() throws Exception {

        mockMvc
                .perform(get("/registration"))
                .andExpect(view().name("user-registration"));

    }

    @Test
    public void shouldProcessRegistration() throws Exception {

        User unsaved = new User();
        unsaved.setFirstName("First_name");
        unsaved.setLastName("Last_name");
        unsaved.setEmail("Email");
        unsaved.setUsername("Username");
        unsaved.setPassword("Password");
        unsaved.setConfirmPassword("Password");
        unsaved.setPhone("3424234234");

        User saved = new User();
        saved.setId(24L);
        saved.setFirstName("First_name");
        saved.setLastName("Last_name");
        saved.setEmail("Email");
        saved.setUsername("Username");
        saved.setPassword("Password");
        saved.setConfirmPassword("Password");
        saved.setPhone("3424234234");

        when(userService.add(unsaved)).thenReturn(saved);

        mockMvc.perform(post("/registration")
                .param("firstName", "First_name")
                .param("lastName", "Last_name")
                .param("email", "Email")
                .param("username", "Username")
                .param("password", "Password")
                .param("confirmPassword", "Password")
                .param("phone", "3424234234"))
                .andExpect(redirectedUrl("/profile/"));

        verify(userService, atLeastOnce()).add(unsaved);
    }

}
