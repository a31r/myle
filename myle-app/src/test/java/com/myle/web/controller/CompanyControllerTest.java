package com.myle.web.controller;

import com.myle.core.model.Company;
import com.myle.core.service.CompanyService;
import com.myle.web.config.WebConfig;
import com.myle.web.util.ConfigUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Test class for {@link CompanyController}
 *
 * @author Amirkhon Bahodurov
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
        @ContextConfiguration(classes = CompanyControllerTest.RootConfig.class),
        @ContextConfiguration(classes = WebConfig.class)
})
public class CompanyControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyController companyController;

    private MockMvc mockMvc;

    @Configuration
    static class RootConfig {

        @Bean
        public CompanyService companyService() {
            return mock(CompanyService.class);
        }
    }

    @Before
    public void setup() throws Exception {

        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .addFilter(springSecurityFilterChain)
                .build();

        ConfigUtil.verifyRootWacSupport(wac, companyService, companyController, springSecurityFilterChain);

    }

    @Test
    public void testShowCompanyRegistrationPage() throws Exception {

        mockMvc
                .perform(get("/company/registration")
                        .with(user("user")))
                .andExpect(view().name("company-registration"));

    }

    @Test
    public void testProcessCompanyRegistration() throws Exception {

        Company unsaved = new Company();
        unsaved.setName("Company");

        Company saved = new Company();
        saved.setName("Company");
        saved.setId(1L);

        when(companyService.add(unsaved)).thenReturn(saved);

        mockMvc
                .perform(post("/company/registration")
                        .with(user("user"))
                        .param("name", "Company"))
                .andExpect(redirectedUrl("/"+saved.getId()+"/profile"));

        verify(companyService, atLeastOnce()).add(unsaved);

    }

    @Test
    public void testShowCompanyProfilePage() throws Exception {

        mockMvc
                .perform(get("/company/1/profile").with(user("user")))
                .andExpect(view().name("company-profile"));

    }

}
